import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';

// import native ----------->
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { Device } from '@ionic-native/device';
import { Sim } from '@ionic-native/sim';
import { AbsoluteDragDirective } from '../directives/absolute-drag/absolute-drag';
// import native <-----------

import { MyApp } from './app.component';
import { RestProvider } from '../providers/rest/rest';
import { UserinfoProvider } from '../providers/userinfo/userinfo';
import { DeviceInfoProvider } from '../providers/device-info/device-info';

@NgModule({
  declarations: [
    MyApp,
    AbsoluteDragDirective
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      platforms: {
        ios: {
          statusbarPadding: true
        }
      }
    }),
    HttpModule,
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Device,
		Sim,
    File,
    Transfer,
    Camera,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestProvider,
    Geolocation,
    UserinfoProvider,
    DeviceInfoProvider
  ]
})
export class AppModule {}
