import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, AlertController, IonicApp, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//import { TabsPage } from '../pages/tabs/tabs';  //for lazy loading...

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
	@ViewChild(Nav) nav: Nav;
  rootPage = 'LoadingPage';

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public ionicApp: IonicApp,
    public app: App,
    public alertCtrl: AlertController,) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    // Handle the default mobile's back button with Confirmation Alert
    this.platform.registerBackButtonAction((event) => {
			// check overlayview(ex, alert, actionSheet..)
      const overlayView = this.app._appRoot._overlayPortal._views[0];
      if (overlayView && overlayView.dismiss) {
        overlayView.dismiss();
      } else {
					// check page is loading, modal, toast or overlay
        let activePortal = this.ionicApp._loadingPortal.getActive() ||
                            this.ionicApp._modalPortal.getActive() ||
                            this.ionicApp._toastPortal.getActive() ||
                            this.ionicApp._overlayPortal.getActive();
        if(activePortal && activePortal.index === 0) {
          /* closes modal */
          activePortal.dismiss();
        } else {
          if(this.nav.getActive().name == 'TabsPage') {    // your homepage
            let confirm = this.alertCtrl.create({
              title: '앱을 종료하시겠습니까?',
              buttons: [
                {
                  text: '취소',
                  handler: () => {
                    console.log('Disagree clicked');
                  }
                },
                {
                  text: '확인',
                  handler: () => {
                    console.log('Agree clicked');
                    this.platform.exitApp();
                  }
                }
              ]
            });
            confirm.present();
          } else {
            if(this.nav.canGoBack())
              this.nav.pop();
            //this.nav.setRoot(Homepage);
          }
        }
      }
    },101);
    
  }
}
