import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { UserinfoProvider } from '../../providers/userinfo/userinfo';

@Injectable()
export class RestProvider {
  
  sclubURL = 'http://localhost:8080/mobile/';
  imgUrl = 'http://localhost:8080/mobile/img/';

  category: any = [];

  constructor(public http: Http,
              public userInfo: UserinfoProvider) {
    console.log('Hello RestProvider Provider');
  }

  getCategory() {
    let queryParams = 'GET_mode=all';
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "getCategory.php";

    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => {    // 'result'=>true, 'category'=>$o
      this.category = data.category;
      for (let i = 0; i < this.category.length; i++) {
        this.category[i].cc_image = this.imgUrl+this.category[i].cc_name+'/'+this.category[i].cc_image;
        this.category[i].cc_pitch_image = this.imgUrl+this.category[i].cc_name+'/'+this.category[i].cc_pitch_image;
      }
    });
  }

  getClubs(queryParams) {
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "getClub.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  }

  getMyClubs(queryParams) {
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "getMyClubs.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  }

  CRUD_Club(queryParams){ 
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "CRUD_Club.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  };

  getMessages(queryParams) {
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "getMessage.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  }

  CRUD_Message(queryParams) {
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "CRUD_Message.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  }

  getMatchs(selYearMonth) {
    console.log('rest.ts selYearMonth==> '+selYearMonth);
    let queryParams = 'GET_mode=cloudAll&selYearMonth='+selYearMonth;
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "getMatch.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  };

  getSchedule(queryParams) {
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "getSchedule.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  };


  CRUD_Schedule(queryParams){ 
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "CRUD_Schedule.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  };

  getMembers(queryParams){ 
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "getMember.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  };

  CRUD_Member(queryParams){ 
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "CRUD_Member.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  };

  getTeams(queryParams){ 
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "getTeam.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  };

  getTeamPosition(queryParams){ 
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "getTeamPosition.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  };

  getPositionInfo(queryParams) {
    let body     : string   = queryParams,
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "getPositionInfo.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  }  

  getMemberRole() {
    let body     : string   = '',
        type     : string   = "application/x-www-form-urlencoded; charset=UTF-8",
        headers  : any      = new Headers({ 'Content-Type': type}),
        options  : any      = new RequestOptions({ headers: headers }),
        url      : any      = this.sclubURL + "getMemberRole.php";
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  } 
}