import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';


@Injectable()
export class UserinfoProvider {

  //-------------------------------------------------> for Is sclub user?
  // u_email, u_password, u_temp_pw, u_name, u_phone_no, u_birthday
  userProfile: any = [
    { u_email:        '' },
    { u_password:     '' },
    { u_name:         '' },
    { u_phone_no:     '' },
    { u_birthday:     '' },
    { u_photo:        '' },
    { u_type:         '' },    //--> 1: sc_flag, 2: fb_flag, 3: gg_flag, 4: nv_flag, 5: kk_flag,
    { sc_flag:        false},  //--> sclub
    { fb_flag:        false},  //--> facebook
    { gg_flag:        false},  //--> google
    { nv_flag:        false},  //--> naver
    { kk_flag:        false},  //--> kakao
    { login_flag:     false},
    { isMember_flag:  false}
  ];

  constructor(public http: Http, public storage: Storage, ) {
    console.log('Hello UserinfoProvider Provider');
  }

  getUser() {
    this.storage.get('sclubUserProfile').then(data => {
      if(data != null) {  //--------------------------------------------------------------------------->>>>> 회원 login

        this.userProfile.u_email =  data.u_email;
        this.userProfile.u_password =  data.u_password;
        this.userProfile.u_name =  data.u_name;
        this.userProfile.u_phone_no =  data.u_phone_no;
        this.userProfile.u_birthday =  data.u_birthday;
        this.userProfile.u_photo =  data.u_photo;
        this.userProfile.u_type =  data.u_type;
        this.userProfile.sc_flag =  data.sc_flag;
        this.userProfile.fb_flag =  data.fb_flag;
        this.userProfile.gg_flag =  data.gg_flag;
        this.userProfile.nv_flag =  data.nv_flag;
        this.userProfile.kk_flag =  data.kk_flag;
        this.userProfile.login_flag =  data.login_flag;
        this.userProfile.isMember_flag =  data.isMember_flag;
        
        console.log('Get userInfo.ts ==> u_email : '+this.userProfile.u_email);
        console.log('Get userInfo.ts ==> u_password: '+this.userProfile.u_password);
        console.log('Get userInfo.ts ==> u_name: '+this.userProfile.u_name);
        console.log('Get userInfo.ts ==> u_phone_no: '+this.userProfile.u_phone_no);
        console.log('Get userInfo.ts ==> u_birthday: '+this.userProfile.u_birthday);
        console.log('Get userInfo.ts ==> u_photo: '+this.userProfile.u_photo);
        console.log('Get userInfo.ts ==> u_type: '+this.userProfile.u_type);
        console.log('Get userInfo.ts ==> login_flag: '+this.userProfile.login_flag);
        console.log('Get userInfo.ts ==> isMember_flag: '+this.userProfile.isMember_flag);
        
      } else {      ////--------------------------------------------------------------------------->>>>> 비회원 login
        //console.log('Get Error from userProfile');
        // non-member log-in
        this.storage.set('sclubUserProfile', this.userProfile).then(()=>{
          console.log('Save userProfile to Storage for non-user');
        },(e)=>{
          console.log('Unable to save',e);
        })  //====> store userProfile
      }
    });
    
  }

//store the profile
  setUser(setParams) {

    this.userProfile.u_email =  setParams.u_email;
    this.userProfile.u_password =  setParams.u_password;
    this.userProfile.u_name =  setParams.u_name;
    this.userProfile.u_phone_no =  setParams.u_phone_no;
    this.userProfile.u_birthday =  setParams.u_birthday;
    this.userProfile.u_photo =  setParams.u_photo;
    this.userProfile.u_type =  setParams.u_type; 
    this.userProfile.login_flag =  setParams.login_flag; 
    this.userProfile.isMember_flag =  setParams.isMember_flag; 

    this.storage.set('sclubUserProfile', this.userProfile).then(()=>{
      console.log('Save userProfile to Storage');
    },(e)=>{
      console.log('Unable to save',e);
    })  //====> store userProfile
  }  

  //this case is user logout, reset userProfile flag to false 
  resetProfile(){
   this.userProfile.login_flag =  false;
   this.storage.set('sclubUserProfile', this.userProfile).then(()=>{
      console.log('Save userProfile to Storage for Logout ');
    },(e)=>{
      console.log('Unable to save',e);
    })  //====> store userProfile
  }  

}
