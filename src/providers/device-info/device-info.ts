import { Injectable } from '@angular/core';
import { Device } from '@ionic-native/device';
import { Sim } from '@ionic-native/sim';
import { Platform, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

/*
  Generated class for the DeviceInfoProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DeviceInfoProvider {

  //private builderURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/builder/url/";
  queryParams: any;

  deviceInfo: any = [
    { d_UUID:     '' },
    { d_Model:    '' },
    { d_Platform: '' },
    { d_width:    '' },
    { d_height:   '' },
    { d_width_r:  '' },
    { d_height_r: '' },
    { d_Type:     '' },
    { d_pushKey:  '' },
    { s_version:  '' },
    { s_link:  '' }
  ];

  d_Type: any = '';
  s_version: any = '';
  s_link: any = '';

  //-------------------------------------------------> for SIM
  carrierName: any;                     // i,a
  countryCode: any;                     // i,a
  mcc: any;                             // i,a
  mnc: any;                             // i,a
  callState: any;                       // a
  dataActivity: any;                    // a
  networkType: any;                     // a
  phoneType: any;                       // a
  simState: any;                        // a
  isNetworkRoaming: any;                // a
  phoneCount: any;                      // a
  activeSubscriptionInfoCount: any;     // a
  activeSubscriptionInfoCountMax: any;  // a
  phoneNumber: any;                     // a
  deviceId: any;                        // a
  deviceSoftwareVersion: any;           // a
  simSerialNumber: any;                 // a
  subscriberId: any;                    // a
  cards: any = [];                      // a

  constructor(
    public platform: Platform, 
    public device: Device,
    public sim: Sim,
    public http: Http,  
    public storage: Storage,
    public alertCtrl: AlertController,) {
    //this.platform.ready().then(() => {
      console.log('Hello DeviceProvider Provider');
    //});
  }

  getSimInfo() {
    this.sim.hasReadPermission().then(
      (info) => {
        console.log('Has permission:', info);
      }
    );

    this.sim.requestReadPermission().then(
      () => console.log('Permission granted'),
      () => console.log('Permission denied')
    );

    this.sim.getSimInfo().then(
      (info) => {
        console.log('Sim info: '+info);
        console.log('this.carrierName: ' +info.carrierName);
        console.log('this.countryCode: ' +info.countryCode);
        console.log('this.mcc: ' +info.mcc);
        console.log('this.mnc: ' +info.mnc);
        console.log('this.callState: ' +info.callState);
        console.log('this.dataActivity: ' +info.dataActivity);
        console.log('this.networkType: ' +info.networkType);
        console.log('this.phoneType: ' +info.phoneType);
        console.log('this.simState: ' +info.simState);
        console.log('this.isNetworkRoaming: ' +info.isNetworkRoaming);
        console.log('this.phoneCount: ' +info.phoneCount);
        console.log('this.activeSubscriptionInfoCount: ' +info.activeSubscriptionInfoCount);
        console.log('this.activeSubscriptionInfoCountMax: ' +info.activeSubscriptionInfoCountMax);
        console.log('this.phoneNumber: ' +info.phoneNumber);
        console.log('this.deviceId: ' +info.deviceId);
        console.log('this.deviceSoftwareVersion: ' +info.deviceSoftwareVersion);
        console.log('this.simSerialNumber: ' +info.simSerialNumber);
        console.log('this.subscriberId: ' +info.subscriberId);
        console.log('this.cards: ' +info.cards);
      },
      (err) => console.log('Unable to get sim info: ', err)
    );
    
  }

  load() {

    if(this.device.platform === 'iOS') {
      this.d_Type = '2';  //iOS
      //this.deviceInfo.d_pushKey = 'iOS';
    } else {
      this.d_Type = '1';  //Android
      //this.deviceInfo.d_pushKey = '935015026479'; //phonegap-plugin-push SENDER_ID
    }

    this.deviceInfo.d_UUID = this.device.uuid;
    this.deviceInfo.d_Model = this.device.model;
    this.deviceInfo.d_Platform = this.device.platform;
    this.deviceInfo.d_Type = this.d_Type;

    console.log('Width: ' + this.platform.width());
    console.log('Height: ' + this.platform.height());
    this.deviceInfo.d_width = this.platform.width();
    this.deviceInfo.d_height = this.platform.height();

    let d_default_w = 360;
    let d_default_h = 615;

    this.deviceInfo.d_width_r = (this.deviceInfo.d_width / d_default_w).toFixed(4);
    this.deviceInfo.d_height_r = (this.deviceInfo.d_height / d_default_h).toFixed(4);
    
    /*
    this.queryParams = 'device='+this.d_Type; 
    let body   : string	 = this.queryParams,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.builderURI + 'get_version.php';
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          console.log('Failed!! get_version()');
        } else {
          console.log('Success!! get_version()');
          this.deviceInfo.s_version = data.version.version;
          this.deviceInfo.s_link = data.version.link;
          console.log('Device app version==> '+this.deviceInfo.s_version+' - '+this.deviceInfo.s_link);
        } 
    });
    */
    this.storage.set('deviceInfo', this.deviceInfo).then(()=>{
      console.log('Save deviceInfo to Storage');
    },(e)=>{
      console.log('Unable to save',e);
    }) 
    console.log('DeviceProvider Provider is loaded');

  }
}