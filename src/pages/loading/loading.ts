import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { DeviceInfoProvider } from '../../providers/device-info/device-info';
import { RestProvider } from '../../providers/rest/rest';
import { UserinfoProvider } from '../../providers/userinfo/userinfo';

@IonicPage()
@Component({
  selector: 'page-loading',
  templateUrl: 'loading.html',
})
export class LoadingPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public platform: Platform,
    public deviceInfo: DeviceInfoProvider,
    public rest: RestProvider,
    public userInfo: UserinfoProvider,
    private storage: Storage,) {
    
    //this.storage.remove('sclubIntro').then(() => {});
    //console.log('----> sclubIntro.storage.remove');
    this.platform.ready().then(() => {
      this.deviceInfo.load();
      this.userInfo.getUser();
      this.rest.getCategory();
    });
  }

  ionViewDidEnter() {
    this.storage.get('sclubIntro').then((val) => {
      console.log('sclubIntro value: '+val);
      if (val == 0) {
        this.navCtrl.setRoot('TabsPage');
      } else {
        this.navCtrl.push('IntroPage');
      }
    }, () => {
      this.navCtrl.push('IntroPage');
    });
  }

}
