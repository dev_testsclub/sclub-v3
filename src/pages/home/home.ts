import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, ModalController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { UserinfoProvider } from '../../providers/userinfo/userinfo';

@IonicPage()  //for lazy loading
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  imgUrl = 'http://localhost:8080/mobile/img/';
  query_params: any;

  myClubs: any = [];
  /* return data of getMyClubs
    // from myclubs
    $t->mc_email = $row['mc_email'];
    $t->mc_cc_id = $row['mc_cc_id'];				
    $t->mc_club_id = $row['mc_club_id'];
    $t->mc_club_name = $row['mc_club_name'];
    // from club
    $t->club_location = $c_row['club_location'];
    $t->club_loc_lat = $c_row['club_loc_lat'];
    $t->club_loc_lng = $c_row['club_loc_lng'];
    $t->club_address = $c_row['club_address'];
    $t->club_emblem = $c_row['club_emblem'];
    $t->club_intro = $c_row['club_intro'];
    $t->club_open_flag = $c_row['club_open_flag'];
    $t->club_contact_email = $c_row['club_contact_email'];
    $t->club_regurations = $c_row['club_regurations'];
    $t->member_cnt = $cnt_row['count(*)'];
  */
  
  login_flag: boolean;
  selctedCategory: any;


  
  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public rest: RestProvider,
    public userInfo: UserinfoProvider) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  ionViewDidEnter() {
    this.getMyClubs();
    console.log('ionViewDidLoad HomePage');
  }

  getMyClubs() {
    this.login_flag = this.userInfo.userProfile.login_flag;
    this.query_params = 'GET_mode='     +'all'+
                        '&mc_email='    +this.userInfo.userProfile.u_email;

    this.rest.getMyClubs(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          this.myClubs = data.myClubs;
          for (let i = 0; i < this.myClubs.length; i++) {
            if(this.myClubs[i].club_emblem != 'NaN') {
              this.myClubs[i].club_emblem = this.imgUrl+'club_img/'+this.myClubs[i].mc_club_id+'/'+this.myClubs[i].club_emblem;
            }
          }
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get getMyClubs Complete')
    );
  }
  
  searchClub() {

  }  

  addMyClub() {
    if(!this.login_flag) {
      this.login();
    } else {
      let alert = this.alertCtrl.create();
      alert.setTitle('Select Category');

      alert.addInput({
        type: 'checkbox',
        label: 'Football',
        value: '8'
      });

      alert.addInput({
        type: 'checkbox',
        label: 'Baseball',
        value: '10'
      });

      alert.addInput({
        type: 'checkbox',
        label: 'Basketball',
        value: '11'
      });

      alert.addInput({
        type: 'checkbox',
        label: 'Tennisball',
        value: '12'
      });

      alert.addButton('Cancel');
      alert.addButton({
        text: 'OK',
        handler: data => {
          console.log('Checkbox data:'+data);
          this.selctedCategory = data;

          //-------------> Create or Join Club
          let alert = this.alertCtrl.create();
          alert.setTitle('Create/Join Club');
          
          alert.addInput({
            type: 'checkbox',
            label: 'Create my Club',
            value: 'create'
          });
          
          alert.addInput({
            type: 'checkbox',
            label: 'Join the Club',
            value: 'join'
          });
          
          alert.addButton('Cancel');
          alert.addButton({
            text: 'OK',
            handler: data => {
              console.log('Checkbox data:'+data);
              if(data == 'create') {
                  let ClubPage_modal = this.modalCtrl.create('ClubPage',{
                    cc_id: this.selctedCategory,
                    club_id: '999999'
                  });
                  ClubPage_modal.onDidDismiss(data => {       
                    if (data == true) {
                      console.log('CreateClubPage return: '+data);
                      //this.navCtrl.setRoot(this.navCtrl.getActive().component);
                      this.getMyClubs();
                    }
                  });
                  ClubPage_modal.present();
              } else {
                  let JoinClubPage_modal = this.modalCtrl.create('JoinClubPage',{
                    cc_id: this.selctedCategory
                  });
                  JoinClubPage_modal.onDidDismiss(data => {       
                    if (data == true) {
                      console.log('JoinClubPage return: '+data);
                      //this.navCtrl.setRoot(this.navCtrl.getActive().component);
                      this.getMyClubs();
                    }
                  });
                  JoinClubPage_modal.present();
              }
            }
          });
          alert.present();
          //-------------> Create or Join Club
        }
      });
      alert.present();
    }
  }

  selectedClub(selectedClub) {
    let BoardPage_modal = this.modalCtrl.create('BoardPage',{
      club_id: selectedClub
    });
    BoardPage_modal.present();
  }

  login() {
    let LoginPage_modal = this.modalCtrl.create('LoginPage');
    LoginPage_modal.onDidDismiss(data => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
    });
    LoginPage_modal.present();
  }

}
