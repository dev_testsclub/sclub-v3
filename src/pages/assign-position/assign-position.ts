import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ModalController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-assign-position',
  templateUrl: 'assign-position.html',
})
export class AssignPositionPage {
  imgUrl = 'http://localhost:8080/mobile/img/';
  query_params: any;
  cc_id: any;
  club_id: any;

  members: any = [];
  /*
  $t->m_id = $row->m_id; 
  $t->m_email = $row->m_email; 
  $t->m_nick = $row->m_nick; 
  $t->m_pp_id = $row->m_pp_id; 
  $t->m_cp_id = $row->m_cp_id; 
  $t->m_back_no = $row->m_back_no; 
  $t->m_image = $row->m_image;
  $t->m_role_id = $row->m_role_id; 
  $t->m_admin = $row->m_admin; 
  $t->m_birthday = $row->m_birthday; 
  $t->m_phone = $row->m_phone; 
  $t->m_alarm = $row->m_alarm;
  $t->club_id = $row->club_id;
  
  $t->u_name = $row->u_name;	// member name
  $t->mrole_name = $row->mrole_name;	// member role name
  $t->u_birthday = $row->u_birthday;	// member birthday

  $t->m_pp_name = $p_row['p_full_name'];
  $t->m_cp_name = $c_row['p_full_name'];
*/
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public rest: RestProvider,
    public modalCtrl: ModalController,) {
      this.cc_id = this.navParams.get("cc_id");
      this.club_id = this.navParams.get("club_id");
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MembersPage');
  }

  ionViewDidEnter() {
    this.getMembers();
    console.log('ionViewDidEnter MembersPage');
  }

  getMembers() {
    this.query_params = 'club_id='+this.club_id;
    this.rest.getMembers(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          this.members = data.members;
          for (let i = 0; i < this.members.length; i++) {
            if(this.members[i].m_image != 'NaN') {
              this.members[i].m_image = this.imgUrl+'club_img/'+this.members[i].club_id+'/'+this.members[i].m_image;
            }
          }
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get getMembers Complete')
    );
  }

  setPosition(m_id) {
    let SetPositionPage_modal = this.modalCtrl.create('SetPositionPage',{
      club_id: this.club_id,
      m_id: m_id
    });
    SetPositionPage_modal.onDidDismiss(data => {
      if(data == true) {
        this.getMembers();
      }
    });
    SetPositionPage_modal.present();
  }
  
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
