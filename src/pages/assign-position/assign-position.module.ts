import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignPositionPage } from './assign-position';

@NgModule({
  declarations: [
    AssignPositionPage,
  ],
  imports: [
    IonicPageModule.forChild(AssignPositionPage),
  ],
  exports: [
    AssignPositionPage
  ]
})
export class AssignPositionPageModule {}
