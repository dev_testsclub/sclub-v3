import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-match',
  templateUrl: 'match.html',
})
export class MatchPage {

  imgUrl = 'http://localhost:8080/mobile/img/';
  category: string = "football";  // for [(ngModel)]
  
  matchs: any = [];
  footballMatchs: any = [];    // cc_id:"8"  football
  baseballMatchs: any = [];    // cc_id:"10" baseball
  basketballMatchs: any = [];  // cc_id:"11" basketball
  tennisMatchs: any = [];      // cc_id:"12" tennis

  /*////////////
  address:"대한민국 경기도 용인시 수지구 성복동 성복2로78번길 51"
  cc_id:"8"
  club_id:"1"
  contents:"성복FC  3 VS 2  신구FC"
  divisionName:"대회 경기"
  end:"10:00:00"
  hTeamName:"성복FC"
  methodName:"Home"
  oTeamName:null
  oppUserName:null
  reqUserName:"Kim Sooyoung"
  start:"2017-01-08  09:00:00"
  summary:"Waiting for Match."

  mb_address:"대한민국 경기도 성남시 중원구 금광2동 광명로 377"
  mb_confirm:"x"
  mb_division:"3"
  mb_end_date:"2017-01-08"
  mb_end_time:"10:00:00"
  mb_id:"14"
  mb_loc_lat:"37.4488075"
  mb_loc_lng:"127.16783720000001"
  mb_location:"신구대학교"
  mb_method:"1"
  mb_opp_club_id:"0"
  mb_opp_ok:"w"
  mb_opp_email:""
  mb_req:"o"
  mb_req_memo:"대회출전 테스트"
  mb_req_email:"010-3312-1234"
  mb_start_date:"2017-01-08"
  mb_start_time:"09:00:00"
  mb_title:"성남시 주최 대회 출전"

  mh_division:"1"
  mh_divisionName:"예선전"
  mh_name:"신구대학배 축구대회"
  mh_opp_name:"신구FC"
  mh_outcome:"win"
  mh_seq:"1
  /////////////*/

 	calMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  calDaysForMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  selectedYear: any; 
  selectedMonth: any;
  selectedDate: any;
  selYearMonth: any;

  CurrentDate = new Date();
  UICalendarDisplay: any = [
    { Date: false },
    { Month: false },
    { Year: false }
  ];

  datesDisp: any = [[],[],[],[],[],[]];
  displayYear: any;
  dislayMonth: any;
  shortMonth: any;
  displayMonthYear: any;
  displayDate: any;
  dateformat:any = "="; 
  date_array: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest: RestProvider) {
  }

  ionViewDidLoad() {
    this.selectedYear = this.CurrentDate.getFullYear(),
    this.selectedMonth = this.CurrentDate.getMonth(),
    this.selectedDate = this.CurrentDate.getDate();
    this.UICalendarDisplay.Date = true;
    this.UICalendarDisplay.Month = false;
    this.UICalendarDisplay.Year = false; 

    //let todayDate = new Date().toISOString().slice(0,7); //dateFormat --> '2017-08'
    //this.rest.getMatchs(todayDate);

    this.displayCompleteDate();
    this.displayMonthCalendar();  
    console.log('ionViewDidLoad MatchPage');
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad HomePage');
  }

  //ngOnInit() {}

  displayCompleteDate() {
    //let timeStamp = new Date(this.selectedYear, this.selectedMonth, this.selectedDate).getTime();
    let format = '';
    if(this.dateformat == undefined) {
      format = "dd - MMM - yy";
    } else {
      format = this.dateformat;
    }
  };

  selectedMonthPrevClick() {
    this.selectedDate = 1;
    if(this.selectedMonth == 0) {
      this.selectedMonth = 11;
      this.selectedYear--;
    } else {
      this.dislayMonth = this.selectedMonth--;
    }
    this.displayMonthCalendar();
  };

  selectedMonthNextClick() {
    this.selectedDate = 1;
    if(this.selectedMonth == 11) {
      this.selectedMonth = 0;
      this.selectedYear++;
    } else {
      this.dislayMonth = this.selectedMonth++;
    }
    this.displayMonthCalendar();
  };

  selectedDateClick(date) {
    this.displayDate = date.date;
    this.selectedDate = date.date;

    if(date.type == 'newMonth') {
      let mnthDate = new Date(this.selectedYear, this.selectedMonth, 32)
      this.selectedMonth = mnthDate.getMonth();
      this.selectedYear = mnthDate.getFullYear();
      this.displayMonthCalendar();
    } else if(date.type == 'oldMonth') {
      let mnthDate = new Date(this.selectedYear, this.selectedMonth, 0);
      this.selectedMonth = mnthDate.getMonth();
      this.selectedYear = mnthDate.getFullYear();
      this.displayMonthCalendar();
    };     
    this.displayCompleteDate();
  };

  selectedCategory(){
    this.displayMonthCalendar();
    this.displayMonthCalendar(); 
  }

  displayMonthCalendar() {
      let displayMonthCalendarFlag: boolean = false;
      let dateDisp = [[],[],[],[],[],[]];
      let countDatingStart = 1;
      let endingDateLimit = this.calDaysForMonth[this.selectedMonth];
      if(this.calMonths[this.selectedMonth] === 'February') {
        if(this.selectedYear%4 === 0) {
          endingDateLimit = 29;
        } else {
          endingDateLimit = 28;
        }
      } 

      this.displayYear = this.selectedYear;
      this.dislayMonth = this.calMonths[this.selectedMonth];
      this.shortMonth = this.calMonths[this.selectedMonth].slice(0,3);
      this.displayMonthYear = this.calMonths[this.selectedMonth]+' '+this.selectedYear;
      this.displayDate = this.selectedDate;

      let startDay = new Date(this.selectedYear, this.selectedMonth, 1).getDay();
      let nextMonthStartDates = 1;
      let prevMonthLastDates = new Date(this.selectedYear, this.selectedMonth, 0).getDate();

      let selYear = this.displayYear;
      let selMonth = this.selectedMonth + 1;
      selYear = selYear.toString();
      
      if(selMonth > 9) {
        selMonth = selMonth.toString();
        this.selYearMonth = selYear +'-'+ selMonth;  // combine string
      } else {
        selMonth = selMonth.toString();
        this.selYearMonth = selYear +'-0'+ selMonth;  // combine string 
      };

      this.rest.getMatchs(this.selYearMonth).subscribe(
          data => {

            this.matchs = [];
            this.footballMatchs = [];    // cc_id:"8"  football
            this.baseballMatchs = [];    // cc_id:"10" baseball
            this.basketballMatchs = [];  // cc_id:"11" basketball
            this.tennisMatchs = [];      // cc_id:"12" tennis

            this.matchs = data.matchs;
            for (let i = 0; i < this.matchs.length; i++) {
              if(this.matchs[i].reqUserImage == 'empty.png' || this.matchs[i].reqUserImage == null) {
                this.matchs[i].reqUserImage = 'NaN';
              } else {
                this.matchs[i].reqUserImage = this.imgUrl+'club_img/'+this.matchs[i].club_id+'/'+this.matchs[i].reqUserImage;
              }
            }
            for (let i = 0; i < this.matchs.length; i++) {
              if(this.matchs[i].cc_id == '8') {              // 8:football
                this.footballMatchs.push(this.matchs[i]);
              } else if(this.matchs[i].cc_id == '10') {      // 10:baseball
                this.baseballMatchs.push(this.matchs[i]);
              } else if(this.matchs[i].cc_id == '11') {      // 11:basketball
                this.basketballMatchs.push(this.matchs[i]);
              } else {                                      // 12:tennis
                this.tennisMatchs.push(this.matchs[i]);
              }
            }

            this.date_array = [];
            if(this.category == 'football') {
              for (let i=0; i<this.footballMatchs.length; i++){
                  this.date_array[i] = {'date' : parseInt(this.footballMatchs[i].mb_start_date.substr(8,2))};
              }
            } else if(this.category == 'baseball') {
              for (let i=0; i<this.baseballMatchs.length; i++){
                  this.date_array[i] = {'date' : parseInt(this.baseballMatchs[i].mb_start_date.substr(8,2))};
              }
            } else if(this.category == 'basketball') {
              for (let i=0; i<this.basketballMatchs.length; i++){
                  this.date_array[i] = {'date' : parseInt(this.basketballMatchs[i].mb_start_date.substr(8,2))};
              }
            } else {
              for (let i=0; i<this.tennisMatchs.length; i++){
                  this.date_array[i] = {'date' : parseInt(this.tennisMatchs[i].mb_start_date.substr(8,2))};
              }
            }

            for(let i=0; i<6; i++) {
              if (displayMonthCalendarFlag === false) {
                displayMonthCalendarFlag = true;
                for(let j=0; j<7; j++) {
                  if(j < startDay) {
                    dateDisp[i][j] = {"type":"oldMonth", "color":"white", "date":(prevMonthLastDates - startDay + 1)+j};
                    this.datesDisp = dateDisp;
                  } else {
                    dateDisp[i][j] = {"type":"currentMonth", "color":"black", "date":countDatingStart++};
                    for (let m=0; m<this.date_array.length; m++){
                      if(dateDisp[i][j].date == this.date_array[m].date){
                        dateDisp[i][j] = {"type":"currentMonth", "color":"red", "date":this.date_array[m].date};
                      }
                    }
                    this.datesDisp = dateDisp;
                  }
                }
              } else {
                for(let k=0; k<7; k++) {
                  if(countDatingStart <= endingDateLimit) {
                    dateDisp[i][k] = {"type":"currentMonth", "color":"black", "date":countDatingStart++};
                    for (let m=0; m<this.date_array.length; m++){
                      if(dateDisp[i][k].date == this.date_array[m].date){
                        dateDisp[i][k] = {"type":"currentMonth", "color":"red", "date":this.date_array[m].date};
                      }
                    }
                    this.datesDisp = dateDisp;
                  } else {
                    dateDisp[i][k] = {"type":"newMonth", "color":"white", "date":nextMonthStartDates++};
                    this.datesDisp = dateDisp;
                  }
                }
              }
            }
          },
          err => {
              console.log(err);
          },
          () => console.log('Get match Complete')
      );
  } 

}
