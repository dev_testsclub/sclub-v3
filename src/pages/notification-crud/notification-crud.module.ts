import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationCrudPage } from './notification-crud';

@NgModule({
  declarations: [
    NotificationCrudPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificationCrudPage),
  ],
  exports: [
    NotificationCrudPage
  ]
})
export class NotificationCrudPageModule {}
