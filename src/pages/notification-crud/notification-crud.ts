import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { UserinfoProvider } from '../../providers/userinfo/userinfo';

@IonicPage()
@Component({
  selector: 'page-notification-crud',
  templateUrl: 'notification-crud.html',
})
export class NotificationCrudPage {


  imgUrl = 'http://localhost:8080/mobile/img/';
  
  cc_id: any;
  club_id: any;

  /*
		$msg_division=$_POST['msg_division'];
		$msg_memo=$_POST['msg_memo'];
		$msg_like_cnt = 0;
		$msg_attach=$_POST['msg_attach']; '0': No attached msg_file(image), '1': Attached msg_file(image)
		$msg_file=$_POST['msg_file'];
		$msg_email=$_POST['msg_email']; 
		$club_id=$_POST['club_id'];
  */

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController, 
    public rest: RestProvider,
    public userInfo: UserinfoProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationCrudPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
