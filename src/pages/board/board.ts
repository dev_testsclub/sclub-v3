import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { UserinfoProvider } from '../../providers/userinfo/userinfo';

@IonicPage()
@Component({
  selector: 'page-board',
  templateUrl: 'board.html',
})
export class BoardPage {
  
  imgUrl = 'http://localhost:8080/mobile/img/';
  segment: string = "message";  
  club_id: any;
  
  //for message >>>>>>>>>>>>>>>>>>>>
  message_query_params: any;
  general_msgs: any = []; //msg_division = '1'; //general messages
  notices_msgs: any = []; //msg_division = '2'; //notices messages
  /* return data of getMessage
    club_id:"1"
    creator_image:"memberImg_icksykim@gmail.com_1479426098652.jpg"
    creator_name:"Kim Sooyoung"
    msg_attach:"1"
    msg_date:"2017-01-27"
    msg_division:"1"
    msg_file:"msgImg_icksykim@gmail.com_1485505043792.jpg"
    msg_id:"63"
    msg_like_cnt:"1"
    msg_memo:"우리조카 김주혜"
    msg_email:"010-3312-1234"
    */
    // "http://localhost:8080/mobile/img/club_img/{{m.club_id}}/{{m.creator_image}}"
    // "http://localhost:8080/mobile/img/club_img/{{m.club_id}}/{{m.msg_file}}"
  //for message <<<<<<<<<<<<<<<<<<<<<
  
  //for schedule >>>>>>>>>>>>>>>>>>>>
  schedule: any = [];
  /*
  attend_cnt:"0"
  club_id:"1"
  comment_cnt:"0"
  created:"2017-02-02 12:47:05"
  description:"Friendly match with CE FC"
  divisionName:"Friend match"
  end_date:"2017-02-01"
  end_time:"11:54:00"
  g_address:"대한민국 경기도 용인시 수지구 성복동 성복2로78번길 51"
  g_division:"2"
  g_loc_lat:"37.3196525"
  g_loc_lng:"127.07639459999996"
  g_location:"성복중학교"
  g_method:"1"
  g_preparation:" "
  id:"51"
  methodName:"Home"
  non_attend_cnt:"0"
  non_response_cnt:7
  register_image:"memberImg_icksykim@gmail.com_1479426098652.jpg"
  register_name:"Kim Sooyoung"
  register_email:"010-3312-1234"
  start_date:"2017-02-01"
  start_time:"11:54:00"
  title:"Friendly match :: CE FC"
  */

 	calMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  calDaysForMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  selectedYear: any; 
  selectedMonth: any;
  selectedDate: any;
  selYearMonth: any;

  CurrentDate = new Date();
  UICalendarDisplay: any = [
    { Date: false },
    { Month: false },
    { Year: false }
  ];

  datesDisp: any = [[],[],[],[],[],[]];
  displayYear: any;
  dislayMonth: any;
  shortMonth: any;
  displayMonthYear: any;
  displayDate: any;
  dateformat:any = "="; 
  date_array: any = [];
  //for schedule <<<<<<<<<<<<<<<<<<<

  //for position >>>>>>>>>>>>>>>>>>>>
  //for position 
  //for dues <<<<<<<<<<<<<<<<<<<
  //for dues >>>>>>>>>>>>>>>>>>>>
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController, 
    public rest: RestProvider,
    public userInfo: UserinfoProvider) {
      this.club_id = navParams.get("club_id");
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BoardPage');
  }

  ionViewDidEnter() {
    // for message
    this.getMessages();
    this.getNotices();

    // for schedule
    this.selectedYear = this.CurrentDate.getFullYear(),
    this.selectedMonth = this.CurrentDate.getMonth(),
    this.selectedDate = this.CurrentDate.getDate();
    this.UICalendarDisplay.Date = true;
    this.UICalendarDisplay.Month = false;
    this.UICalendarDisplay.Year = false; 
    this.displayCompleteDate();
    this.displayMonthCalendar();
    console.log('ionViewDidEnter BoardPage');
  }

  getMessages() {
    this.message_query_params = 'GET_mode='       +'eachClub'+ 
                                '&club_id='       +this.club_id+
                                '&msg_division='	+'1'+ // '1' : general message 
                                '&req_switch='	  +'1';	// '1' : request from messageBoard
    this.rest.getMessages(this.message_query_params).subscribe(
      data => {
        if(data.result == true) {
          this.general_msgs = data.message_board;
          for (let i = 0; i < this.general_msgs.length; i++) {
            if(this.general_msgs[i].creator_image == 'empty.png') {
              this.general_msgs[i].creator_image = 'NaN';
            } else {
              this.general_msgs[i].creator_image = this.imgUrl+'club_img/'+this.general_msgs[i].club_id+'/'+this.general_msgs[i].creator_image;
            }
            if(this.general_msgs[i].msg_file == 'empty.png') {
              this.general_msgs[i].msg_file = 'NaN';
            } else {
              this.general_msgs[i].msg_file = this.imgUrl+'club_img/'+this.general_msgs[i].club_id+'/'+this.general_msgs[i].msg_file;
            }
          }
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get getGeneralMessage Complete')
    );
  }
  getNotices() {
    this.message_query_params = 'GET_mode='       +'eachClub'+ 
                                '&club_id='       +this.club_id+
                                '&msg_division='	+'2'+ // '2' : notice 
                                '&req_switch='	  +'1';	// '1' : request from messageBoard
    this.rest.getMessages(this.message_query_params).subscribe(
      data => {
        if(data.result == true) {
          this.notices_msgs = data.message_board;
          for (let i = 0; i < this.notices_msgs.length; i++) {
            if(this.notices_msgs[i].creator_image == 'empty.png') {
              this.notices_msgs[i].creator_image = 'NaN';
            } else {
              this.notices_msgs[i].creator_image = this.imgUrl+'club_img/'+this.notices_msgs[i].club_id+'/'+this.notices_msgs[i].creator_image;
            }
            if(this.notices_msgs[i].msg_file == 'empty.png') {
              this.notices_msgs[i].msg_file = 'NaN';
            } else {
              this.notices_msgs[i].msg_file = this.imgUrl+'club_img/'+this.notices_msgs[i].club_id+'/'+this.notices_msgs[i].msg_file;
            }
          }
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get getGeneralMessage Complete')
    );
  }

  displayCompleteDate() {
    //let timeStamp = new Date(this.selectedYear, this.selectedMonth, this.selectedDate).getTime();
    let format = '';
    if(this.dateformat == undefined) {
      format = "dd - MMM - yy";
    } else {
      format = this.dateformat;
    }
  };

  selectedMonthPrevClick() {
    this.selectedDate = 1;
    if(this.selectedMonth == 0) {
      this.selectedMonth = 11;
      this.selectedYear--;
    } else {
      this.dislayMonth = this.selectedMonth--;
    }
    this.displayMonthCalendar();
  };

  selectedMonthNextClick() {
    this.selectedDate = 1;
    if(this.selectedMonth == 11) {
      this.selectedMonth = 0;
      this.selectedYear++;
    } else {
      this.dislayMonth = this.selectedMonth++;
    }
    this.displayMonthCalendar();
  };

  selectedDateClick(date) {
    this.displayDate = date.date;
    this.selectedDate = date.date;

    if(date.type == 'newMonth') {
      let mnthDate = new Date(this.selectedYear, this.selectedMonth, 32)
      this.selectedMonth = mnthDate.getMonth();
      this.selectedYear = mnthDate.getFullYear();
      this.displayMonthCalendar();
    } else if(date.type == 'oldMonth') {
      let mnthDate = new Date(this.selectedYear, this.selectedMonth, 0);
      this.selectedMonth = mnthDate.getMonth();
      this.selectedYear = mnthDate.getFullYear();
      this.displayMonthCalendar();
    };     
    this.displayCompleteDate();
  };

  selectedCategory(){
    this.displayMonthCalendar();
    this.displayMonthCalendar(); 
  }

  displayMonthCalendar() {
      let displayMonthCalendarFlag: boolean = false;
      let dateDisp = [[],[],[],[],[],[]];
      let countDatingStart = 1;
      let endingDateLimit = this.calDaysForMonth[this.selectedMonth];
      if(this.calMonths[this.selectedMonth] === 'February') {
        if(this.selectedYear%4 === 0) {
          endingDateLimit = 29;
        } else {
          endingDateLimit = 28;
        }
      } 

      this.displayYear = this.selectedYear;
      this.dislayMonth = this.calMonths[this.selectedMonth];
      this.shortMonth = this.calMonths[this.selectedMonth].slice(0,3);
      this.displayMonthYear = this.calMonths[this.selectedMonth]+' '+this.selectedYear;
      this.displayDate = this.selectedDate;

      let startDay = new Date(this.selectedYear, this.selectedMonth, 1).getDay();
      let nextMonthStartDates = 1;
      let prevMonthLastDates = new Date(this.selectedYear, this.selectedMonth, 0).getDate();

      let selYear = this.displayYear;
      let selMonth = this.selectedMonth + 1;
      selYear = selYear.toString();
      
      if(selMonth > 9) {
        selMonth = selMonth.toString();
        this.selYearMonth = selYear +'-'+ selMonth;  // combine string
      } else {
        selMonth = selMonth.toString();
        this.selYearMonth = selYear +'-0'+ selMonth;  // combine string 
      };
      let queryParams = 'GET_mode=' +'selMonth'+
                        '&club_id='+this.club_id+
                        '&selYearMonth='+this.selYearMonth;
      this.rest.getSchedule(queryParams).subscribe(
          data => {
            this.schedule = [];
            this.schedule = data.schedule;
            for (let i = 0; i < this.schedule.length; i++) {
              if(this.schedule[i].register_image == 'empty.png' || this.schedule[i].register_image == null) {
                this.schedule[i].register_image = 'NaN';
              } else {
                this.schedule[i].register_image = this.imgUrl+'club_img/'+this.schedule[i].club_id+'/'+this.schedule[i].register_image;
              }
            }
            
            this.date_array = [];
              for (let i=0; i<this.schedule.length; i++){
                  this.date_array[i] = {'date' : parseInt(this.schedule[i].start_date.substr(8,2))};
              }


            for(let i=0; i<6; i++) {
              if (displayMonthCalendarFlag === false) {
                displayMonthCalendarFlag = true;
                for(let j=0; j<7; j++) {
                  if(j < startDay) {
                    dateDisp[i][j] = {"type":"oldMonth", "color":"white", "date":(prevMonthLastDates - startDay + 1)+j};
                    this.datesDisp = dateDisp;
                  } else {
                    dateDisp[i][j] = {"type":"currentMonth", "color":"black", "date":countDatingStart++};
                    for (let m=0; m<this.date_array.length; m++){
                      if(dateDisp[i][j].date == this.date_array[m].date){
                        dateDisp[i][j] = {"type":"currentMonth", "color":"red", "date":this.date_array[m].date};
                      }
                    }
                    this.datesDisp = dateDisp;
                  }
                }
              } else {
                for(let k=0; k<7; k++) {
                  if(countDatingStart <= endingDateLimit) {
                    dateDisp[i][k] = {"type":"currentMonth", "color":"black", "date":countDatingStart++};
                    for (let m=0; m<this.date_array.length; m++){
                      if(dateDisp[i][k].date == this.date_array[m].date){
                        dateDisp[i][k] = {"type":"currentMonth", "color":"red", "date":this.date_array[m].date};
                      }
                    }
                    this.datesDisp = dateDisp;
                  } else {
                    dateDisp[i][k] = {"type":"newMonth", "color":"white", "date":nextMonthStartDates++};
                    this.datesDisp = dateDisp;
                  }
                }
              }
            }
          },
          err => {
              console.log(err);
          },
          () => console.log('Get match Complete')
      );
  } 


  myClub() {
    let MyClubPage_modal = this.modalCtrl.create('MyClubPage',{
      club_id: this.club_id
    });
    MyClubPage_modal.present();    
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
