import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
  
import { RestProvider } from '../../providers/rest/rest';
import { UserinfoProvider } from '../../providers/userinfo/userinfo';

@IonicPage()
@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
})
export class SchedulePage {

  imgUrl = 'http://localhost:8080/mobile/img/';  
  cc_id: any;
  club_id: any;

  schedule: any = [];
  /*
  attend_cnt:"0"
  club_id:"1"
  comment_cnt:"0"
  created:"2017-02-02 12:47:05"
  description:"Friendly match with CE FC"
  divisionName:"Friend match"
  end_date:"2017-02-01"
  end_time:"11:54:00"
  g_address:"대한민국 경기도 용인시 수지구 성복동 성복2로78번길 51"
  g_division:"2"
  g_loc_lat:"37.3196525"
  g_loc_lng:"127.07639459999996"
  g_location:"성복중학교"
  g_method:"1"
  g_preparation:" "
  id:"51"
  methodName:"Home"
  non_attend_cnt:"0"
  non_response_cnt:7
  register_image:"memberImg_icksykim@gmail.com_1479426098652.jpg"
  register_name:"Kim Sooyoung"
  register_email:"010-3312-1234"
  start_date:"2017-02-01"
  start_time:"11:54:00"
  title:"Friendly match :: CE FC"
  */ 

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public rest: RestProvider,
    public userInfo: UserinfoProvider) {
      this.cc_id = this.navParams.get("cc_id");
      this.club_id = this.navParams.get("club_id");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SchedulePage');
  }

  ionViewDidEnter() {
    this.getSchedule();
    console.log('ionViewDidEnter SchedulePage');
  }

  getSchedule() {
    let queryParams = 'GET_mode=' +'all'+
                      '&club_id='+this.club_id;
    this.rest.getSchedule(queryParams).subscribe(
      data => {
        this.schedule = [];
        this.schedule = data.schedule;
        for (let i = 0; i < this.schedule.length; i++) {
          if(this.schedule[i].register_image == 'empty.png' || this.schedule[i].register_image == null) {
            this.schedule[i].register_image = 'NaN';
          } else {
            this.schedule[i].register_image = this.imgUrl+'club_img/'+this.schedule[i].club_id+'/'+this.schedule[i].register_image;
          }
        }
      },
      err => {
          console.log(err);
      },
      () => console.log('Get match Complete')
    );
  }

  addSchedule() {
    let ScheduleCrudPagePage_modal = this.modalCtrl.create('ScheduleCrudPage',{
      selectedSchedule: 'add'
    });
    ScheduleCrudPagePage_modal.present(); 
  }

  updateSchedule(selectedSchedule) {
    let ScheduleCrudPagePage_modal = this.modalCtrl.create('ScheduleCrudPage',{
      selectedSchedule: selectedSchedule
    });
    ScheduleCrudPagePage_modal.present();      
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
