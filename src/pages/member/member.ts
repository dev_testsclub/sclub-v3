import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { ModalController, ActionSheetController, LoadingController} from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

import { RestProvider } from '../../providers/rest/rest';
import { UserinfoProvider } from '../../providers/userinfo/userinfo';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-member',
  templateUrl: 'member.html',
})
export class MemberPage {

  sclubURL = 'http://localhost:8080/mobile/';
  imgUrl = 'http://localhost:8080/mobile/img/';
  query_params: any;

  updateFlag: boolean = false;
  selfUpdateFlag: boolean = false;

  memberRole: any = [];
  /*
    $t->mrole_id = $row->mrole_id;
    $t->mrole_name = $row->mrole_name;
    $t->mrole_desc = $row->mrole_desc;
  */
  role_name: any;

  positionInfo: any = [];
  /*
  	$t->p_id = $row->p_id;
		$t->p_short_name = $row->p_short_name;
		$t->p_full_name = $row->p_full_name;
		$t->p_description = $row->p_description;
  */
  cp_full_name: any;
  pp_full_name: any;

  loading: any;
  lastImage: string = null;
  takePictureFlag: boolean = false;
  imgFlag: boolean = false;
  disp_image:  any;
  isBirthday: boolean = false;
  isPhone: boolean = false;
  isAlarm: boolean = false;

  cc_id: any;
  club_id: any;
  m_id: any;
  m_email:  any;
  m_nick:  any;
  m_pp_id:  any;
  m_cp_id:  any;
  m_back_no:  any;
  m_image:  any;
  m_role_id:  any;
  m_admin:  any;
  m_birthday:  any;
  m_phone:  any;
  m_alarm:  any;

  /*
  --> readonly
  'club_id'=>$row['club_id']
  'm_id'=>$row['m_id'], 
  'm_email'=>$row['m_email'], 

  --> create/update for member
  'm_image'=>$row['m_image'],
  'm_nick'=>$row['m_nick'],
  'm_pp_id'=>$row['m_pp_id'], 
  'm_birthday'=>$row['m_birthday'], 
  'm_phone'=>$row['m_phone'], 
  'm_alarm'=>$row['m_alarm'],

  --> create/update for member
  'm_cp_id'=>$row['m_cp_id'], 
  'm_back_no'=>$row['m_back_no'],
  'm_role_id'=>$row['m_role_id'], 
  'm_admin'=>$row['m_admin'],
  */
  constructor(
    public platform: Platform,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    private camera: Camera,
    private transfer: Transfer, 
    private file: File, 
    private filePath: FilePath,
    public rest: RestProvider,
    public userInfo: UserinfoProvider,) {
      this.cc_id = this.navParams.get("cc_id");
      this.club_id = this.navParams.get("club_id");
      this.m_id = this.navParams.get("m_id");
    }

  ionViewDidLoad() {
    this.getPositionInfo();
    this.getMemberRole();
    if(this.m_id == '999999') {
      this.updateFlag = false;   //create member
    } else {
      this.updateFlag = true;    //update member
      this.getMember();
    }
    console.log('ionViewDidLoad MemberPage');
  }

  getMember() {
    this.query_params = 'CRUD_mode='+'read'+
                        '&m_id='+this.m_id;
    this.rest.CRUD_Member(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          this.m_id = data.m_id;

          this.m_email = data.m_email;
          if(this.m_email !== this.userInfo.userProfile.u_email) {
            this.selfUpdateFlag = false;
          } else {
            this.selfUpdateFlag = true;
          }

          this.m_image = data.m_image;
          this.m_nick = data.m_nick;
          this.m_back_no = data.m_back_no;
          this.m_admin = data.m_admin;

          this.m_birthday = data.m_birthday;
          if(this.m_birthday == '1') { this.isBirthday = true; } else { this.isBirthday = false; }
          this.m_phone = data.m_phone; 
          if(this.m_phone == '1') { this.isPhone = true; } else { this.isPhone = false; }
          this.m_alarm = data.m_alarm;
          if(this.m_alarm == '1') { this.isAlarm = true; } else { this.isAlarm = false; }

          this.m_cp_id = data.m_cp_id;
          for(let i=0; i<this.positionInfo.length; i++) {
            if(this.positionInfo[i].p_id == this.m_cp_id) {
              this.cp_full_name = this.positionInfo[i].p_full_name;
            }
          }
          this.m_pp_id = data.m_pp_id; 
          for(let i=0; i<this.positionInfo.length; i++) {
            if(this.positionInfo[i].p_id == this.m_pp_id) {
              this.pp_full_name = this.positionInfo[i].p_full_name;
            }
          }

          this.m_role_id = data.m_role_id;
          for(let i=0; i<this.memberRole.length; i++) {
            if(this.memberRole[i].mrole_id == this.m_role_id) {
              this.role_name = this.memberRole[i].mrole_name;
            }
          }

          if(this.m_image == 'NaN') {
            this.imgFlag = false;
          } else {          
            this.disp_image = this.imgUrl+'club_img/'+this.club_id+'/'+this.m_image;
            this.imgFlag = true;
          }
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get member Complete')
    );
  }

  getMemberRole() {
    //this.query_params = 'cc_id='+this.cc_id;
    this.rest.getMemberRole().subscribe(
      data => {
        if(data.result == true) {
          this.memberRole = data.member_role;
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get getMemberRole Complete')
    );    
  }

  getPositionInfo() {
    this.query_params = 'cc_id='+this.cc_id;
    this.rest.getPositionInfo(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          this.positionInfo = data.position_info;
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get getPositionInfo Complete')
    );    
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    this.takePictureFlag = true;
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
  
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      console.log('Error while selecting image.');
    });
  }

  private createFileName() {
    //Create a new name for the photo
    var d = new Date(),
    n = d.getTime(),
    newFileName =  "clubImg_" + n + ".jpg";  //한글 이름 않됨 !!!!
    return newFileName;
  }
 
  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      console.log('Error while storing file.');
    });
  }
  
  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  selectedPposition(e) { 
    console.log('m_pp_id:' +this.m_pp_id);
  }

  checkBirthday() {
    if(this.isBirthday == true) { 
      this.m_birthday = '1';
    } else {
      this.m_birthday = '0';
    }
  }
  checkPhone() {
    if(this.isPhone == true) { 
      this.m_phone = '1';
    } else {
      this.m_phone = '0';
    }
  }
  checkAlarm() {
    if(this.isAlarm == true) { 
      this.m_alarm = '1';
    } else {
      this.m_alarm = '0';
    }
  }  

  selectedRole(e) { 
    console.log('m_role_id:' +this.m_role_id);
  }

  submitUpdate() {
    if(this.takePictureFlag == true) {
      // Destination URL
      var url = this.sclubURL+"CRUD_Member.php";
    
      // File for Upload
      var targetPath = this.pathForImage(this.lastImage);
      console.log('Image targetPath : ',targetPath);
      // File name only
      var filename = this.lastImage;
      console.log('Image filename : ',filename);
      var serverImageDirectory = 'img/club_img/'+this.club_id

      var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : {'directory': serverImageDirectory, 
                  'fileName': filename,
                  'CRUD_mode': 'update',
                  'm_id': this.m_id, 
                  'm_email': this.m_email,
                  'm_nick': this.m_nick,
                  'm_pp_id': this.m_pp_id,
                  'm_cp_id': this.m_cp_id,
                  'm_back_no': this.m_back_no,
                  'm_image': this.lastImage,
                  'm_role_id': this.m_role_id,
                  'm_birthday': this.m_birthday,
                  'm_phone': this.m_phone,
                  'm_alarm': this.m_alarm,
                  'club_id': this.club_id
                }
      };
    
      const fileTransfer: TransferObject = this.transfer.create();
    
      this.loading = this.loadingCtrl.create({
        content: 'Uploading...',
      });
      this.loading.present();
    
      // Use the FileTransfer to upload the image with input params
      fileTransfer.upload(targetPath, url, options).then(data => {
        this.loading.dismissAll()
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: '정상 처리되었습니다.',
            buttons: ['확인']
          });
          alert.present();
          this.viewCtrl.dismiss(true);
      }, err => {
        this.loading.dismissAll()
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '처리에 실패하였습니다.',
          buttons: ['확인']
        });
        alert.present();
        console.log('Error while uploading file.');
      });
    } else {
      this.query_params = 'CRUD_mode='+'update'+
                          '&m_id='+this.m_id+
                          '&m_email='+this.m_email+
                          '&m_nick='+this.m_nick+
                          '&m_pp_id='+this.m_pp_id+
                          '&m_cp_id='+this.m_cp_id+
                          '&m_back_no='+this.m_back_no+
                          '&m_image='+this.m_image+
                          '&m_role_id='+this.m_role_id+
                          '&m_birthday='+this.m_birthday+
                          '&m_phone='+this.m_phone+
                          '&m_alarm='+this.m_alarm+
                          '&club_id='+this.club_id;
      this.rest.CRUD_Member(this.query_params).subscribe(
        data => {
          if(data.result == true) {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: data.msg,
              buttons: ['확인']
            });
            alert.present();
            this.viewCtrl.dismiss(true);
          } else {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: data.msg,
              buttons: ['확인']
            });
            alert.present();
          }
        },
          err => {
            console.log(err);
          },
        () => console.log('Update member Complete')
      ); 
    }
  }

  submitCreate() {
    if(this.takePictureFlag == true) {
      // Destination URL
      var url = this.sclubURL+"CRUD_Member.php";
    
      // File for Upload
      var targetPath = this.pathForImage(this.lastImage);
      console.log('Image targetPath : ',targetPath);
      // File name only
      var filename = this.lastImage;
      console.log('Image filename : ',filename);
      var serverImageDirectory = 'img/club_img/'+this.club_id

      var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : {'directory': serverImageDirectory, 
                  'fileName': filename,
                  'CRUD_mode': 'create',
                  'm_email': this.m_email,
                  'm_nick': this.m_nick,
                  'm_pp_id': this.m_pp_id,
                  'm_cp_id': this.m_cp_id,
                  'm_back_no': this.m_back_no,
                  'm_image': this.lastImage,
                  'm_role_id': this.m_role_id,
                  'm_birthday': this.m_birthday,
                  'm_phone': this.m_phone,
                  'm_alarm': this.m_alarm,
                  'club_id': this.club_id
                }
      };
    
      const fileTransfer: TransferObject = this.transfer.create();
    
      this.loading = this.loadingCtrl.create({
        content: 'Uploading...',
      });
      this.loading.present();
    
      // Use the FileTransfer to upload the image with input params
      fileTransfer.upload(targetPath, url, options).then(data => {
        this.loading.dismissAll()
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '정상 처리되었습니다.',
          buttons: ['확인']
        });
        alert.present();
        this.viewCtrl.dismiss(true);
      }, err => {
        this.loading.dismissAll()
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '처리에 실패하였습니다.',
          buttons: ['확인']
        });
        alert.present();
        console.log('Error while uploading file.');
      });
    } else {
      this.query_params = 'CRUD_mode='+'create'+
                          '&m_email='+this.m_email+
                          '&m_nick='+this.m_nick+
                          '&m_pp_id='+this.m_pp_id+
                          '&m_cp_id='+this.m_cp_id+
                          '&m_back_no='+this.m_back_no+
                          '&m_image='+this.m_image+
                          '&m_role_id='+this.m_role_id+
                          '&m_birthday='+this.m_birthday+
                          '&m_phone='+this.m_phone+
                          '&m_alarm='+this.m_alarm+
                          '&club_id='+this.club_id;
      this.rest.CRUD_Member(this.query_params).subscribe(
        data => {
          if(data.result == true) {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: data.msg,
              buttons: ['확인']
            });
            alert.present();
            this.viewCtrl.dismiss(true);
          } else {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: data.msg,
              buttons: ['확인']
            });
            alert.present();
          }
        },
          err => {
            console.log(err);
          },
        () => console.log('Create member Complete')
      ); 
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
