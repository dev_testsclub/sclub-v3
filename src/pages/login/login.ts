import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, ViewController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';

import { UserinfoProvider } from '../../providers/userinfo/userinfo';
import { RestProvider } from '../../providers/rest/rest';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  //-------------------------------------------------> for data query
  queryParams: any;
  sclubURL = 'http://localhost:8080/mobile/';
  imgUrl = 'http://localhost:8080/mobile/img/';

  //-------------------------------------------------> loginParams for userInfo
  loginParams: any = [
    { u_email:        '' },
    { u_password:     '' },
    { u_name:         '' },
    { u_phone_no:     '' },
    { u_birthday:     '' },
    { u_photo:        '' },
    { u_type:         '' },   //--> 1: sc_flag, 2: fb_flag, 3: gg_flag, 4: nv_flag, 5: kk_flag,
    { sc_flag:        false}, //--> sclub
    { fb_flag:        false}, //--> facebook
    { gg_flag:        false}, //--> google
    { nv_flag:        false}, //--> naver
    { kk_flag:        false},  //--> kakao
    { login_flag:     false},
    { isMember_flag:  false}
  ];

  FB_APP_ID: number = 412877125738651;

  login_flag: boolean;
  isMember_flag: boolean;
 
  u_name: any;
  u_email: any;
  u_password: any;

  constructor(
    public viewCtrl           : ViewController, 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public http               : Http,
    public loadingCtrl        : LoadingController,
    public alertCtrl          : AlertController,
    public modalCtrl          : ModalController,
    public rest               : RestProvider,
    public userInfo           : UserinfoProvider) {

    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  ionViewDidEnter() {
    this.login_flag = this.userInfo.userProfile.login_flag;
    this.isMember_flag = this.userInfo.userProfile.isMember_flag;
    this.u_name = this.userInfo.userProfile.u_name;
    this.u_email = this.userInfo.userProfile.u_email;
    console.log('this.login_flag ='+this.userInfo.userProfile.login_flag);
    console.log('this.isMember_flag ='+ this.userInfo.userProfile.isMember_flag);
    this.rest.getMyClubs(this.userInfo.userProfile.u_email);
  }

  DoMemberLogin(){
    //console.log('this.email: '+ this.email);
    if(this.u_email == undefined || this.u_password == undefined) {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '이메일, 비밀번호를 입력하세요.',
          buttons: ['확인']
      });
      alert.present();
    } else {
                      
      this.queryParams = 'CRUD_mode='     +'check'+
                         '&u_email='      +this.u_email+
                         '&u_password='   +this.u_password;
      let body   : string	 = this.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.sclubURL + 'CRUD_User.php';
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result == false) {
            console.log('Failed!! 회원 로그인 ==>' +data.result);
            let alert = this.alertCtrl.create({
                title: '알림',
                subTitle: '등록되지 않은 이메일입니다.',
                buttons: ['확인']
              });
              alert.present();
          } else {
            this.loginParams.u_name = data.u_name;
            this.loginParams.u_phone_no = data.u_phone_no;
            this.loginParams.u_email = data.u_email;
            this.loginParams.u_password = data.u_password;
            this.loginParams.u_birthday = data.u_birthday;
            this.loginParams.u_type = '1';  //--> 1: sc_flag, 2: fb_flag, 3: gg_flag, 4: nv_flag, 5: kk_flag
            this.loginParams.sc_flag = true;
            this.loginParams.login_flag = true;
            this.loginParams.isMember_flag = true;
            this.userInfo.setUser(this.loginParams);
            console.log('Success!! 회원 로그인');
            this.viewCtrl.dismiss(this.loginParams.login_flag);
          }
      });
    }
  }


  findPassword() {
    if(this.u_email == undefined) {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '등록된 이메일을 입력하세요.',
          buttons: ['확인']
      });
      alert.present();
    } else if(this.u_email != this.userInfo.userProfile.u_email) {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '등록되지 않은 이메일입니다.',
          buttons: ['확인']
      });
      alert.present();
    } else {
      //send new password to user E-mail 
    }
  }

/*
  //페이스북 로그인
  DoFbLogin() {
    let permissions = new Array();
    //the permissions your facebook app needs from the user
    //permissions = ['public_profile', 'user_friends', 'email'];
    permissions = ["public_profile", "email"];
    let env = this;

    env.facebook.login(['public_profile', 'user_friends', 'email'])
    .then((res: FacebookLoginResponse) => {
          env.facebook.api('me?fields=email,name', null).then(
          (profileData) => {  

            //if(profileData.email == 'icksykim@naver.com') {
            //  profileData.email = 'icksykim2@naver.com';
            //}

              console.log('facebook email + ==>'+profileData.email);
              let picture = "https://graph.facebook.com/" + res.authResponse.userID + "/picture?type=large";
              let facebook_pw = env.GetPasswordEncrypted(res.authResponse.accessToken);
              facebook_pw = facebook_pw.replace(/\+/g,''); //특정문자('+') 제거
              
              if(profileData.email == undefined) {
                let alert = env.alertCtrl.create({
                  title: '알림',
                  subTitle: '확인되지 않은 Facebook email입니다.',
                  buttons: ['확인']
                });
                alert.present();
              } else {

                  env.queryParams = 'email='          +profileData.email+
                                    '&pw='            +facebook_pw+
                                    '&type='          +'2'+                   //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                                    '&push_key='      +env.deviceProvider.deviceInfo.d_pushKey+
                                    '&photo='         +picture+ 
                                    '&push_enable='   +'YES'+                                       //push enable 1: YES, 2: NO
                                    '&device='        +env.deviceProvider.deviceInfo.d_Type+
                                    '&device_num='    +env.deviceProvider.deviceInfo.d_UUID+
                                    '&version='       +env.deviceProvider.deviceInfo.s_version; 
                  let body   : string	 = env.queryParams,
                      type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                      headers: any		 = new Headers({ 'Content-Type': type}),
                      options: any 		 = new RequestOptions({ headers: headers }),
                      url 	 : any		 = env.memberURI + "set_member_login.php";
                  env.http.post(url, body, options)
                  .map(res => res.json())
                  .subscribe(data => { 
                      if (data.result != 'ok') {

                          env.queryParams = 'email=' + profileData.email +  //user.email
                                '&pw=' + facebook_pw + 
                                '&push_key=' +env.deviceProvider.deviceInfo.d_pushKey+  //env.deviceProvider.deviceInfo.d_pushKey
                                '&type=' +'2'+ //1:차체, 2:페이스북, 3:구글, 4:카카오, 5:기타
                                '&name=' + profileData.name+
                                '&photo=' + picture +
                                '&device=' +env.deviceProvider.deviceInfo.d_Type+  //env.deviceProvider.deviceInfo.d_Type
                                '&device_num=' +env.deviceProvider.deviceInfo.d_UUID+  //env.deviceProvider.deviceInfo.d_UUID
                                '&phone_num=' +'facebook'+
                                '&version=' +env.deviceProvider.deviceInfo.s_version;

                          let body   : string	 = env.queryParams,
                              type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                              headers: any		 = new Headers({ 'Content-Type': type}),
                              options: any 		 = new RequestOptions({ headers: headers }),
                              url 	 : any		 = env.memberURI + "set_member_join.php";
                          env.http.post(url, body, options)
                          .map(res => res.json())
                          .subscribe(data => { 
                              if (data.result != 'ok') {
                                let alert = env.alertCtrl.create({
                                  title: '알림',
                                  subTitle: env.msgService.getMessage(data.result),
                                  buttons: ['확인']
                                });
                                alert.present();
                              } else { 
                                //now we have the users info, let's save it in the ionicStorage thru userInfo
                                env.loginParams.loginApp = 'Facebook';
                                env.loginParams.u_name = profileData.name;
                                env.loginParams.u_phone_num = 'facebook';
                                env.loginParams.u_email = profileData.email;
                                env.loginParams.u_pw = facebook_pw; 
                                env.loginParams.u_photo = picture;
                                env.loginParams.u_type = '2';   //--> 1: sc_flag, 2: fb_flag, 3: gg_flag, 4: nv_flag, 5: kk_flag
                                env.loginParams.fb_flag = true;
                                env.loginParams.login_flag = true;
                                env.loginParams.isMember_flag = true;
                                env.userInfo.setUser(env.loginParams);
                                env.viewCtrl.dismiss(env.loginParams.login_flag);
                              } //====> (data.result != 'ok') "set_member_join.php"
                          });

                      } else { 
                        env.loginParams.loginApp = 'Facebook';
                        env.loginParams.u_name = profileData.name;
                        env.loginParams.u_phone_num = 'facebook';
                        env.loginParams.u_email = profileData.email;
                        env.loginParams.u_pw = facebook_pw; 
                        env.loginParams.u_photo = picture;
                        env.loginParams.u_type = '2';  //가입경로 1: 1000Under, 2: Facebook, 3: Ka//--> 1: sc_flag, 2: fb_flag, 3: gg_flag, 4: nv_flag, 5: kk_flag
                        env.loginParams.fb_flag = true;
                        env.loginParams.login_flag = true;
                        env.loginParams.isMember_flag = true;
                        env.userInfo.setUser(env.loginParams);
                        env.viewCtrl.dismiss(env.loginParams.login_flag);
                      } //====> (data.result == 'ok') "set_member_login.php"
                  }); 

              } // check profileData.email
              
          },(err) => {
            console.log(JSON.stringify(err));
            let alert = env.alertCtrl.create({
              title: '알림',
              subTitle: 'Facebook 연동에 실패하였습니다.',
              buttons: ['확인']
            });
            alert.present();
          });
    }).catch(e => console.log('Error logging into Facebook' + e));
  }
*/

/*
  //구글 로그인
  DoGoogleLogin(){
    let env = this;
    let loading = env.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    env.googlePlus.login({
      'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
      'webClientId': '674574852016-i1m0v4jq6qgqdcqr5m501f6r1c7hp3h2.apps.googleusercontent.com',  // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.r iOS
      'offline': true
    })
    .then(function (user) {
      loading.dismiss();

      //if(user.email == 'icksykim@gmail.com') {
      //  user.email = 'sykim2@gmail.com';
      //}

      console.log('user_data.userId : '+user.userId);
      console.log('user_data.displayName : '+user.displayName);
      console.log('user_data.email : '+user.email);
      console.log('user_data.imageUrl : '+user.imageUrl);
      console.log('user_data.accessToken : '+user.accessToken); //not response
      //console.log('user_data.idToken : '+user.idToken);

      let google_pw = env.GetPasswordEncrypted(user.accessToken);
      //console.log('facebook pw + ==>'+google_pw);
      google_pw = google_pw.replace(/\+/g,''); //특정문자('+') 제거
      //console.log('facebook pw  ==>'+google_pw);
      
      env.queryParams = 'email='          +user.email+
                        '&pw='            +google_pw+
                        '&type='          +'4'+                   //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                        '&push_key='      +env.deviceProvider.deviceInfo.d_pushKey+
                        '&photo='         +user.imageUrl+ 
                        '&push_enable='   +'YES'+                                       //push enable 1: YES, 2: NO
                        '&device='        +env.deviceProvider.deviceInfo.d_Type+
                        '&device_num='    +env.deviceProvider.deviceInfo.d_UUID+
                        '&version='       +env.deviceProvider.deviceInfo.s_version;

      console.log('env.queryParams ==>'+env.queryParams);

      let body   : string	 = env.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = env.memberURI + "set_member_login.php";
      env.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          console.log('data.result ==>'+data.result);
          if (data.result != 'ok') {
              env.queryParams = 'email=' +user.email+
                                '&pw=' +google_pw+
                                '&push_key=' +env.deviceProvider.deviceInfo.d_pushKey+  //env.deviceProvider.deviceInfo.d_pushKey
                                '&type=' +'4'+              //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                                '&name=' +user.displayName+
                                '&photo=' +user.imageUrl+
                                '&device=' +env.deviceProvider.deviceInfo.d_Type+  //env.deviceProvider.deviceInfo.d_Type
                                '&device_num=' +env.deviceProvider.deviceInfo.d_UUID+  //env.deviceProvider.deviceInfo.d_UUID
                                '&phone_num=' +'google'+
                                '&version=' +env.deviceProvider.deviceInfo.s_version;
              let body   : string	 = env.queryParams,
                  type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                  headers: any		 = new Headers({ 'Content-Type': type}),
                  options: any 		 = new RequestOptions({ headers: headers }),
                  url 	 : any		 = env.memberURI + "set_member_join.php";
              env.http.post(url, body, options)
              .map(res => res.json())
              .subscribe(data => { 
                  if (data.result != 'ok') {
                    let alert = env.alertCtrl.create({
                      title: '알림',
                      subTitle: env.msgService.getMessage(data.result),
                      buttons: ['확인']
                    });
                    alert.present();
                  } else {
                    //now we have the users info, let's save it in the ionicStorage thru userInfo
                    env.loginParams.loginApp = 'Google';
                    env.loginParams.u_name = user.displayName;
                    env.loginParams.u_phone_num = 'google';
                    env.loginParams.u_email = user.email;
                    env.loginParams.u_pw = google_pw; 
                    env.loginParams.u_photo = user.imageUrl;
                    env.loginParams.u_type = '3';   //--> 1: sc_flag, 2: fb_flag, 3: gg_flag, 4: nv_flag, 5: kk_flag
                    env.loginParams.gg_flag = true;
                    env.loginParams.login_flag = true;
                    env.loginParams.isMember_flag = true;
                    env.userInfo.setUser(env.loginParams);
                    env.viewCtrl.dismiss(env.loginParams.login_flag);
                  } 
              }); //====> (data.result != 'ok') "set_member_join.php"

          } else { 
            env.loginParams.loginApp = 'Google';
            env.loginParams.u_name = user.displayName;
            env.loginParams.u_phone_num = 'google';
            env.loginParams.u_email = user.email;
            env.loginParams.u_pw = google_pw; 
            env.loginParams.u_photo = user.imageUrl;
            env.loginParams.u_type = '3';     //--> 1: sc_flag, 2: fb_flag, 3: gg_flag, 4: nv_flag, 5: kk_flag
            env.loginParams.gg_flag = true;
            env.loginParams.login_flag = true;
            env.loginParams.isMember_flag = true;
            env.userInfo.setUser(env.loginParams);
            env.viewCtrl.dismiss(env.loginParams.login_flag);
          } //====> (data.result == 'ok') "set_member_login.php"
      });
  
    }, function (error) {
      let alert = env.alertCtrl.create({
        title: '알림',
        subTitle: 'Google 연동에 실패하였습니다.',
        buttons: ['확인']
      });
      alert.present();
      console.log('Google userInfo return error: ' +error);
      loading.dismiss();
    });
    
  }
*/
  signUp() {//Modal
    let SignupPage_modal = this.modalCtrl.create('SignupPage');
    SignupPage_modal.present();
  }  
 dismiss() {
    this.viewCtrl.dismiss(this.login_flag);
 }
}
