import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {
    public options;
    constructor(
        private navCtrl: NavController,
        private storage: Storage,
        public rest: RestProvider) {
            this.options = {
                show: false
            };
            this.rest.getCategory();
    }
 
    doNotShow() {
        if (!this.options.show) {
            //Save flag if don't need to show intro slider on next app startup
            this.storage.set('sclubIntro', 0).then(() => {
                // we don't actually need this callbacks in such simple example,
                // but in real apps it will be useful
            });
        }
        this.navCtrl.setRoot('TabsPage');
    }

    dismiss() {
        this.navCtrl.setRoot('TabsPage');
    }
}
