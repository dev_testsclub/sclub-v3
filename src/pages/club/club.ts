import { Component, ViewChild, ElementRef } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, ViewController, AlertController} from 'ionic-angular';
import { ModalController, ActionSheetController, LoadingController} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

import { UserinfoProvider } from '../../providers/userinfo/userinfo';
import { RestProvider } from '../../providers/rest/rest';

declare var google;
declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-club',
  templateUrl: 'club.html',
})

export class ClubPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  loading: any;
  lastImage: string = null;

  query_params: any;
  sclubURL = 'http://localhost:8080/mobile/';
  imgUrl = 'http://localhost:8080/mobile/img/';

  clubUpdateFlag: boolean = false;
  takePictureFlag: boolean = false;
  /*  case 'create' :
		$_POST['club_name'];
		$_POST['club_location'];
		$_POST['club_address'];
		$_POST['club_loc_lat'];
		$_POST['club_loc_lng'];
		$_POST['club_emblem'];
		$_POST['club_intro'];
		$_POST['club_open_flag'];
		$_POST['club_owner_email'];
		$_POST['club_contact_email'];
		$_POST['club_purcStatus']; <---'x'
		$_POST['club_regurations'];
		$_POST['cc_id'];
  */
  cc_id: any;
  club_id: any;
  club_name: any;
  club_location: any;
  club_loc_lat: number = 0;
  club_loc_lng: number = 0;
  club_address: any;
  club_emblem: any;
  disp_emblem: any;
  clubImgFlag: boolean = false;
  club_intro: any;
  club_owner_email: any;
  club_contact_email: any;
  club_open_flag: any;
  openFlags = [
    { club_open_flag: 'fo', desc: 'Full Open'},
    { club_open_flag: 'lo', desc: 'Midium Open'},
    { club_open_flag: 'no', desc: 'Not Open'},
  ];

  constructor(
    public platform: Platform,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public rest: RestProvider,
    public userInfo: UserinfoProvider,
    private geolocation: Geolocation,
    private camera: Camera,
    private transfer: Transfer, 
    private file: File, 
    private filePath: FilePath,) { 
      this.cc_id = this.navParams.get("cc_id");
      this.club_id = this.navParams.get("club_id");
    }

  ionViewDidLoad() {
    if(this.club_id == '999999') {  //create myClub
      this.clubUpdateFlag = false;
      this.loadMap();
    } else {
      this.clubUpdateFlag = true;   //update myClub
      this.getClub();
      this.loadMap();
    }
    console.log('ionViewDidLoad ClubPage');
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter ClubPage');
  }

  getClub() {
    this.query_params = 'CRUD_mode='+'read'+
                        '&club_id='+this.club_id;
    this.rest.CRUD_Club(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          this.club_name = data.club_name;
          this.club_location = data.club_location;
          this.club_loc_lat = data.club_loc_lat;
          this.club_loc_lng = data.club_loc_lng;
          this.club_address = data.club_address;
          this.club_emblem = data.club_emblem;
          if(this.club_emblem == 'NaN') {
            this.clubImgFlag = false;
          } else {          
            this.disp_emblem = this.imgUrl+'club_img/'+this.club_id+'/'+this.club_emblem;
            this.clubImgFlag = true;
          }

          this.club_intro = data.club_intro;

          this.club_open_flag = data.club_open_flag;  // fo, lo, no
          this.club_owner_email = data.club_owner_email;
          this.club_contact_email = data.club_contact_email;
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
      err => {
          console.log(err);
      },
      () => console.log('Get club Complete')
    );    
  }

  loadMap(){  //-------------------------------------------------------------------------> loadMap()
    this.geolocation.getCurrentPosition().then((position) => {
      if(this.clubUpdateFlag == false) {
        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        let mapOptions = {
          center: latLng,
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          streetViewControl: false,
          fullscreenControl: false
        }
        this.map = new google.maps.Map(document.getElementById('map'), mapOptions);      
      } else {
        let latLng = new google.maps.LatLng(this.club_loc_lat, this.club_loc_lng);
        let mapOptions = {
          center: latLng,
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          streetViewControl: false,
          fullscreenControl: false
        }
        this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
      }
	    let input_from = /** @type {HTMLInputElement} */ (document.getElementById('place_input'));
      let options = {
          types: [],
          componentRestrictions: {country: 'kr'}
      };

      // create the two autocompletes on the place_input fields
      let autocomplete = new google.maps.places.Autocomplete(input_from, options);
      autocomplete.bindTo('bounds', this.map);
      // we need to save a reference to this as we lose it in the callbacks
      let self = this;

      //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      let infowindow = new google.maps.InfoWindow();
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: this.map.getCenter()
      });

      // add the first listener
      autocomplete.addListener('place_changed', function() {
        infowindow.close();
        let place = autocomplete.getPlace();
        if (!place.geometry) {
          return;
        } else {
          self.club_location = place.name;
          self.club_address = place.formatted_address;
          self.club_loc_lat = place.geometry.location.lat();
          self.club_loc_lng = place.geometry.location.lng();
        }
        // Set the position of the marker using the place ID and location.
        marker.setPlace( /** @type {!google.maps.Place} */ ({
            placeId: place.place_id,
            location: place.geometry.location
        }));
        marker.setVisible(true);
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + '<br>' + place.formatted_address);
        infowindow.open(this.map, marker);
      });

    });
  }

  selectedOpenFlag(e) { 
    console.log('club_open_flag:' +this.club_open_flag);
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    this.takePictureFlag = true;
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
  
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      console.log('Error while selecting image.');
    });
  }

  private createFileName() {
    //Create a new name for the photo
    var d = new Date(),
    n = d.getTime(),
    newFileName =  "clubImg_" + n + ".jpg";  //한글 이름 않됨 !!!!
    return newFileName;
  }
 
  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      console.log('Error while storing file.');
    });
  }
  
  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  public submitUpdate() {
    if(this.takePictureFlag == true) {
      // Destination URL
      var url = this.sclubURL+"CRUD_Club.php";
    
      // File for Upload
      var targetPath = this.pathForImage(this.lastImage);
      console.log('Image targetPath : ',targetPath);
      // File name only
      var filename = this.lastImage;
      console.log('Image filename : ',filename);
      var serverImageDirectory = 'img/club_img/'+this.club_id

      var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : {'directory': serverImageDirectory, 
                  'fileName': filename,
                  'CRUD_mode': 'update',
                  'club_id': this.club_id, 
                  'club_name': this.club_name,
                  'club_loc_lat': this.club_loc_lat,
                  'club_loc_lng': this.club_loc_lng,
                  'club_location': this.club_location,
                  'club_address': this.club_address,
                  'club_emblem': this.lastImage,
                  'club_intro': this.club_intro,
                  'club_owner_email': this.club_owner_email,
                  'club_contact_email': this.club_contact_email,                  
                  'club_open_flag': this.club_open_flag
                }
      };
    
      const fileTransfer: TransferObject = this.transfer.create();
    
      this.loading = this.loadingCtrl.create({
        content: 'Uploading...',
      });
      this.loading.present();
    
      // Use the FileTransfer to upload the image
      fileTransfer.upload(targetPath, url, options).then(data => {
        this.loading.dismissAll()
        console.log('Image succesful uploaded.');
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '정상 처리되었습니다.',
          buttons: ['확인']
        });
        alert.present();
      }, err => {
        this.loading.dismissAll()
        console.log('Error while uploading file.');
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '처리에 실패하였습니다.',
          buttons: ['확인']
        });
        alert.present();
      });
    } else {
      this.query_params = 'CRUD_mode='+'update'+
                          '&club_id='+this.club_id+
                          '&club_name='+this.club_name+
                          '&club_loc_lat='+this.club_loc_lat+
                          '&club_loc_lng='+this.club_loc_lng+
                          '&club_location='+this.club_location+
                          '&club_address='+this.club_address+
                          '&club_emblem='+this.club_emblem+
                          '&club_intro='+this.club_intro+
                          '&club_contact_email='+this.club_contact_email+
                          '&club_owner_email='+this.club_owner_email+
                          '&club_open_flag='+this.club_open_flag;
      this.rest.CRUD_Club(this.query_params).subscribe(
        data => {
          if(data.result == true) {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: data.msg,
              buttons: ['확인']
            });
            alert.present();
            this.viewCtrl.dismiss(true);
          } else {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: data.msg,
              buttons: ['확인']
            });
            alert.present();
          }
        },
          err => {
            console.log(err);
          },
        () => console.log('Update club Complete')
      ); 
    }
  }

  submitCreate(){
    if(this.takePictureFlag == true) {
      // Destination URL
      var url = this.sclubURL+"CRUD_Club.php";
    
      // File for Upload
      var targetPath = this.pathForImage(this.lastImage);
      console.log('Image targetPath : ',targetPath);
      // File name only
      var filename = this.lastImage;
      console.log('Image filename : ',filename);
      var serverImageDirectory = 'img/club_img/'+this.club_id

      var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : {'directory': serverImageDirectory, 
                  'fileName': filename,
                  'CRUD_mode': 'create',
                  'cc_id': this.cc_id,
                  'club_name': this.club_name,
                  'club_loc_lat': this.club_loc_lat,
                  'club_loc_lng': this.club_loc_lng,
                  'club_location': this.club_location,
                  'club_address': this.club_address,
                  'club_emblem': this.lastImage,
                  'club_intro': this.club_intro,
                  'club_contact_email': this.club_contact_email,
                  'club_owner_email': this.userInfo.userProfile.u_email,
                  'club_open_flag': this.club_open_flag
                }
      };
    
      const fileTransfer: TransferObject = this.transfer.create();
    
      this.loading = this.loadingCtrl.create({
        content: 'Uploading...',
      });
      this.loading.present();
    
      // Use the FileTransfer to upload the image
      fileTransfer.upload(targetPath, url, options).then(data => {
        this.loading.dismissAll()
        console.log('Image succesful uploaded.');
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '정상 처리되었습니다.',
          buttons: ['확인']
        });
        alert.present();
        this.viewCtrl.dismiss(true);
      }, err => {
        this.loading.dismissAll()
        console.log('Error while uploading file.');
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '처리에 실패하였습니다..',
          buttons: ['확인']
        });
        alert.present();
      });
    } else {
      this.club_emblem = 'NaN';
      this.query_params = 'CRUD_mode='+'create'+
                          '&cc_id='+this.cc_id+
                          '&club_name='+this.club_name+
                          '&club_loc_lat='+this.club_loc_lat+
                          '&club_loc_lng='+this.club_loc_lng+
                          '&club_location='+this.club_location+
                          '&club_address='+this.club_address+
                          '&club_emblem='+this.club_emblem+
                          '&club_intro='+this.club_intro+
                          '&club_contact_email='+this.club_contact_email+
                          '&club_owner_email='+this.userInfo.userProfile.u_email+
                          '&club_open_flag='+this.club_open_flag;
      this.rest.CRUD_Club(this.query_params).subscribe(
        data => {
          if(data.result == true) {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: data.msg,
              buttons: ['확인']
            });
            alert.present();
            this.viewCtrl.dismiss(true);
          } else {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: data.msg,
              buttons: ['확인']
            });
            alert.present();
          }
        },
          err => {
            console.log(err);
          },
        () => console.log('Create club Complete')
      ); 
    }    
  }  

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
