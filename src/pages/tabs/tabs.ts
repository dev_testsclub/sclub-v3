import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'MatchPage';
  tab2Root = 'ClubsPage';
  tab3Root = 'HomePage';
  tab4Root = 'NewFeedPage';
  tab5Root = 'SetupPage';

  constructor() {

  }
}
