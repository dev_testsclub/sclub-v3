import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScheduleCrudPage } from './schedule-crud';

@NgModule({
  declarations: [
    ScheduleCrudPage,
  ],
  imports: [
    IonicPageModule.forChild(ScheduleCrudPage),
  ],
  exports: [
    ScheduleCrudPage
  ]
})
export class ScheduleCrudPageModule {}
