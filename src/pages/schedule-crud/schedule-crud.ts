import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

import { UserinfoProvider } from '../../providers/userinfo/userinfo';
import { RestProvider } from '../../providers/rest/rest';

declare var google;
  /* 
  attend_cnt:"0"
  club_id:"1"
  comment_cnt:"0"
  created:"2017-02-02 12:47:05"
  description:"Friendly match with CE FC"
  divisionName:"Friend match"
  end_date:"2017-02-01"
  end_time:"11:54:00"
  g_address:"대한민국 경기도 용인시 수지구 성복동 성복2로78번길 51"
  g_division:"2"
  g_loc_lat:"37.3196525"
  g_loc_lng:"127.07639459999996"
  g_location:"성복중학교"
  g_method:"1"
  g_preparation:" "
  id:"51"
  methodName:"Home"
  non_attend_cnt:"0"
  non_response_cnt:7
  register_image:"memberImg_icksykim@gmail.com_1479426098652.jpg"
  register_name:"Kim Sooyoung"
  register_phone_no:"010-3312-1234"
  start_date:"2017-02-01"
  start_time:"11:54:00"
  title:"Friendly match :: CE FC"
  */


@IonicPage()
@Component({
  selector: 'page-schedule-crud',
  templateUrl: 'schedule-crud.html',
})
export class ScheduleCrudPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  query_params: any;
  sclubURL = 'http://localhost:8080/mobile/';

  selectedSchedule: any;
  updateFlag: boolean;
  updateEndFlag: boolean = false;
  updateStartFlag: boolean = false;

  divisions = [
    { id: '1', kr_name: '자체경기', en_name: 'Own the game'},
    { id: '2', kr_name: '친선경기', en_name: 'Friendly Match'},
    { id: '3', kr_name: '대회경기', en_name: 'Competition game'}
  ];
  methods = [
    { id: '1', kr_name: '홈', en_name: 'Home'},
    { id: '2', kr_name: '원정', en_name: 'Away'}
  ];

  cc_id: any;
  club_id: any;
  attend_cnt: any;
  comment_cnt: any;
  description: any;
  divisionName: any;
  end_date: any;
  end_time: any;
  end_date_time: any;
  disp_e_date_time: any;
  g_address: any;
  g_division: any;
  g_loc_lat: any;
  g_loc_lng: any;
  g_location: any;
  g_method: any;
  g_preparation: any;
  id: any;
  methodName: any;
  non_attend_cnt: any;
  non_response_cnt: any;
  register_image: any;
  register_name: any;
  register_email: any;
  start_date: any;
  start_time: any;
  start_date_time: any;
  disp_s_date_time: any;
  title: any;
  schedule_created: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    private geolocation: Geolocation,
    public rest: RestProvider,
    public userInfo: UserinfoProvider,) {

  }

  ionViewDidLoad() {
    this.selectedSchedule = this.navParams.get("selectedSchedule");
    if(this.selectedSchedule == 'add') {
      this.updateFlag = false;
      this.loadMap();
    } else {
      this.updateFlag = true;
      this.attend_cnt = this.selectedSchedule.attend_cnt;
      this.club_id = this.selectedSchedule.club_id;
      this.comment_cnt = this.selectedSchedule.comment_cnt;
      this.description = this.selectedSchedule.description;
      this.divisionName = this.selectedSchedule.divisionName;
      this.end_date = this.selectedSchedule.end_date;
      this.end_time = this.selectedSchedule.end_time;
      this.disp_e_date_time = this.end_date+' '+this.end_time.substr(0, 5);
      this.g_address = this.selectedSchedule.g_address.replace("대한민국", "");
      this.g_division = this.selectedSchedule.g_division;
      this.g_loc_lat = this.selectedSchedule.g_loc_lat;
      this.g_loc_lng = this.selectedSchedule.g_loc_lng;
      this.g_location = this.selectedSchedule.g_location;
      this.g_method = this.selectedSchedule.g_method;
      this.g_preparation = this.selectedSchedule.g_preparation;
      this.id = this.selectedSchedule.id;
      this.methodName = this.selectedSchedule.methodName;
      this.non_attend_cnt = this.selectedSchedule.non_attend_cnt;
      this.non_response_cnt = this.selectedSchedule.non_response_cnt;
      this.register_image = this.selectedSchedule.register_image;
      this.register_name = this.selectedSchedule.register_name;
      this.register_email = this.selectedSchedule.register_email;
      this.start_date = this.selectedSchedule.start_date;
      this.start_time = this.selectedSchedule.start_time;
      this.disp_s_date_time = this.start_date+' '+this.start_time.substr(0, 5);
      console.log('this.disp_s_date_time: '+this.disp_s_date_time);
      this.title = this.selectedSchedule.title;
      this.schedule_created = this.selectedSchedule.schedule_created;
      this.loadMap();
    }
    console.log('ionViewDidLoad SchedulePage');
  }

  loadMap(){  //-------------------------------------------------------------------------> loadMap()
    this.geolocation.getCurrentPosition().then((position) => {
      if(this.updateFlag == false) {
        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        let mapOptions = {
          center: latLng,
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          streetViewControl: false,
          fullscreenControl: false
        }
        this.map = new google.maps.Map(document.getElementById('map'), mapOptions);      
      } else {
        let latLng = new google.maps.LatLng(this.g_loc_lat, this.g_loc_lng);
        let mapOptions = {
          center: latLng,
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          streetViewControl: false,
          fullscreenControl: false
        }
        this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
      }
	    let input_from = /** @type {HTMLInputElement} */ (document.getElementById('place_input'));
      let options = {
          types: [],
          componentRestrictions: {country: 'kr'}
      };

      // create the two autocompletes on the place_input fields
      let autocomplete = new google.maps.places.Autocomplete(input_from, options);
      autocomplete.bindTo('bounds', this.map);
      // we need to save a reference to this as we lose it in the callbacks
      let self = this;

      //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      let infowindow = new google.maps.InfoWindow();
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: this.map.getCenter()
      });

      // add the first listener
      autocomplete.addListener('place_changed', function() {
        infowindow.close();
        let place = autocomplete.getPlace();
        if (!place.geometry) {
          return;
        } else {
          self.g_location = place.name;
          self.g_address = place.formatted_address;
          self.g_loc_lat = place.geometry.location.lat();
          self.g_loc_lng = place.geometry.location.lng();
        }
        // Set the position of the marker using the place ID and location.
        marker.setPlace( /** @type {!google.maps.Place} */ ({
            placeId: place.place_id,
            location: place.geometry.location
        }));
        marker.setVisible(true);
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + '<br>' + place.formatted_address);
        infowindow.open(this.map, marker);
      });

    });
  }

  changedDivision(e) {  //-------------------> changedDivision()
    console.log('g_division:' +this.g_division);
  }
  changedMethod(e) {  //---------------------> changedMethod()
    console.log('g_method:' +this.g_method);
  }

  updateStartDate() {
    this.updateStartFlag = !this.updateStartFlag;
  }
  selectedStart(e) {
    console.log('start_date_time: '+this.start_date_time); //2017-01-01T00:00:00Z
    this.start_date = this.start_date_time.substr(0, 10);
    //console.log('this.start_date: '+this.start_date);
    this.start_time = this.start_date_time.substr(11, 8);
    //console.log('this.start_time: '+this.start_time);
  }

  updateEndDate() {
    this.updateEndFlag = !this.updateEndFlag;
  }
  selectedEnd(e) {
    console.log('end_date_time: '+this.end_date_time);
    this.end_date = this.end_date_time.substr(0, 10);
    //console.log('this.start_date: '+this.start_date);
    this.end_time = this.end_date_time.substr(11, 8);
  }

  submitCreate() {
    this.query_params = 'CRUD_mode='+'create'+
                        '&club_id='+this.club_id+
                        '&title='+this.title+
                        '&description='+this.description+
                        '&start_date='+this.start_date+
                        '&start_time='+this.start_time+
                        '&end_date='+this.end_date+
                        '&end_time='+this.end_time+
                        '&g_preparation='+this.g_preparation+
                        '&g_location='+this.g_location+
                        '&g_loc_lat='+this.g_loc_lat+
                        '&g_loc_lng='+this.g_loc_lng+
                        '&g_address='+this.g_address+
                        '&g_division='+this.g_division+
                        '&g_method='+this.g_method+
                        '&register_email='+this.userInfo.userProfile.u_email;
    this.rest.CRUD_Schedule(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
          this.viewCtrl.dismiss(true);
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
        err => {
          console.log(err);
        },
      () => console.log('Create club Complete')
    ); 
  }

  submitUpdate() {
    this.query_params = 'CRUD_mode='+'update'+
                        '&id='+this.id+
                        '&club_id='+this.club_id+
                        '&title='+this.title+
                        '&description='+this.description+
                        '&start_date='+this.start_date+
                        '&start_time='+this.start_time+
                        '&end_date='+this.end_date+
                        '&end_time='+this.end_time+
                        '&g_preparation='+this.g_preparation+
                        '&g_location='+this.g_location+
                        '&g_loc_lat='+this.g_loc_lat+
                        '&g_loc_lng='+this.g_loc_lng+
                        '&g_address='+this.g_address+
                        '&g_division='+this.g_division+
                        '&g_method='+this.g_method+
                        '&register_email='+this.userInfo.userProfile.u_email;
    this.rest.CRUD_Schedule(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
          this.viewCtrl.dismiss(true);
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
        err => {
          console.log(err);
        },
      () => console.log('Create club Complete')
    ); 
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
