import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the SetPositionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-set-position',
  templateUrl: 'set-position.html',
})
export class SetPositionPage {
  @ViewChild('imageCanvas') canvasRef;
  
    image = 'http://upload.wikimedia.org/wikipedia/commons/4/4a/Logo_2013_Google.png';
    cc_id: any;
    club_id: any;
  
    constructor(
      public navCtrl: NavController, 
      public navParams: NavParams,
      public viewCtrl: ViewController,) {
        this.cc_id = this.navParams.get("cc_id");
        this.club_id = this.navParams.get("club_id");
      }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad TeamSetupPage');
    }
    ngAfterViewInit() {
      let canvas = this.canvasRef.nativeElement;
      let context = canvas.getContext('2d');
  
      let source = new Image(); 
      source.crossOrigin = 'Anonymous';
      source.onload = () => {
          canvas.height = source.height;
          canvas.width = source.width;
          context.drawImage(source, 0, 0);
  
          context.font = "100px impact";
          context.textAlign = 'center';
          context.fillStyle = 'black';
          context.fillText(this.cc_id, canvas.width / 2, canvas.height * 0.8);
  
          this.image = canvas.toDataURL();  
      };
      source.src = this.image;
    }
    dismiss() {
      this.viewCtrl.dismiss();
    }
  }
