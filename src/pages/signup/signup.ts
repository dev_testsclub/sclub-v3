import { Component, ViewChild } from '@angular/core';
import { IonicPage, Platform, ViewController, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';

//-----> Providers
//import { DeviceProvider } from '../../providers/device/device';
import { UserinfoProvider } from '../../providers/userinfo/userinfo';

declare var cordova: any;


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  @ViewChild('u_email1') u_email1 ;
  @ViewChild('u_password1') u_password1 ;
  //-------------------------------------------------> for data query
  queryParams: any;
  sclubURL = 'http://localhost:8080/mobile/';

  loginParams: any = [
    { loginApp:     '' },
    { u_email:      '' },
    { u_password:   '' },
    { u_name:       '' },
    { u_phone_no:   '' },
    { u_birthday:   '' },
    { u_photo:      '' },
    { u_type:       ''},    //--> 1: sc_flag, 2: fb_flag, 3: gg_flag, 4: nv_flag, 5: kk_flag,
    { sc_flag:      false}, //--> sclub
    { fb_flag:      false}, //--> facebook
    { gg_flag:      false}, //--> google
    { nv_flag:      false}, //--> naver
    { kk_flag:      false}  //--> kakao
  ];
  
  u_email: any = '';  
  u_password: any = '';
  c_password: any = '';
  u_name: any = '';
  u_phone_no: any = '';
  u_birthday: any = '';

  constructor(
    public viewCtrl: ViewController, 
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public http: Http,
    public platform: Platform,
    public userInfo: UserinfoProvider) {
      //this.userInfo.getProfile();
    }

  ionViewDidLoad() {
    this.u_email1.setFocus();
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    console.log('ionViewDidLoad SignUpPage');
  }

  termsUse(){ //Modal
    //data.policy_list.type == '이용';
    /*
    let termsUsePage_modal = this.modalCtrl.create('TermsUsePage', {termUseContent: this.outerLinkProvider.termUseContent}); 
    termsUsePage_modal.present(); 
    */
  }

  privacyPolicy(){ //Modal
    //data.policy_list.type == '개인';
    /*
    let privacyPolicyPage_modal = this.modalCtrl.create('PrivacyPolicyPage', {privacyPolicyContent: this.outerLinkProvider.privacyPolicyContent}); 
    privacyPolicyPage_modal.present(); 
    */
  }

  u_email_eventHandler(keyCode) {
    if(keyCode == 13) {
      this.u_password1.setFocus();
    }
  }

  submitRegiste(){
      if(this.u_name === '' || this.u_phone_no === '' || this.u_email === '' ||
         this.u_password === '' || this.c_password === '' || this.u_birthday === '') {
          let alert = this.alertCtrl.create({
            title: '알림!',
            subTitle: '모든 내용을 입력해 주십시요.',
            buttons: ['확인']
          });
          alert.present();
      } else if(this.u_password != this.c_password) {
            let alert = this.alertCtrl.create({
              title: '알림!',
              subTitle: '비밀번호"가 올바르지 않습니다.',
              buttons: ['확인']
            });
            alert.present();
      } else {
        let chkPwdResualt = chkPwd(this.u_password);
        if(chkPwdResualt != 'good'){
          let alert = this.alertCtrl.create({
            title: '알림!',
            subTitle: chkPwdResualt,
            buttons: ['확인']
          });
          alert.present();
        } else {  //====> submit logic start.
          // u_email, u_password, u_temp_pw, u_name, u_phone_no, u_birthday 
          this.queryParams = 'CRUD_mode=create'+ 
                              '&u_email=' +this.u_email+
                              '&u_password=' +this.u_password+
                              '&u_name=' +this.u_name+
                              '&u_birthday=' +this.u_birthday+
                              '&u_phone_no=' +this.u_phone_no;

          let body   : string	 = this.queryParams,
              type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
              headers: any		 = new Headers({ 'Content-Type': type}),
              options: any 		 = new RequestOptions({ headers: headers }),
              url 	 : any		 = this.sclubURL + "CRUD_User.php";
          this.http.post(url, body, options)
          .map(res => res.json())
          .subscribe(data => { 
              if (data.result == false) {
                let alert = this.alertCtrl.create({
                  title: '알림',
                  subTitle: data.msg,
                  buttons: ['확인']
                });
                alert.present();
                this.viewCtrl.dismiss(this.loginParams.sc_flag);
              } else {
                this.loginParams.loginApp = 'sclub';
                this.loginParams.u_name = this.u_name;
                this.loginParams.u_phone_no = this.u_phone_no;
                this.loginParams.u_birthday = this.u_birthday;
                this.loginParams.u_email = this.u_email;
                this.loginParams.u_password = this.u_password; 
                this.loginParams.u_photo = 'NaN';
                this.loginParams.u_type = '1';   //--> 1: sc_flag, 2: fb_flag, 3: gg_flag, 4: nv_flag, 5: kk_flag
                this.loginParams.sc_flag = true;
                this.loginParams.login_flag = true;
                this.loginParams.isMember_flag = true;
                this.userInfo.setUser(this.loginParams);
                if(this.userInfo.userProfile.sc_flag == true) {
                  let alert = this.alertCtrl.create({
                    title: '알림',
                    subTitle: '회원 등록이 완료되었습니다.',
                    buttons: ['확인']
                  });
                  alert.present();
                  this.viewCtrl.dismiss(this.loginParams.sc_flag);  //---> Closed Modal SignUpPage and goto ProfilePage
                } else {
                  console.log('Unable to save userInfo.userProfile');
                }
              } //====> (data.result == 'ok')
          });

        };  //====> submit logic end.
      }

      function chkPwd(str){
        let pw = str;
        let num = pw.search(/[0-9]/g);
        let eng = pw.search(/[a-z]/ig);
        //let spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

        if(pw.length < 6 || pw.length > 15){
          return '비밀번호는 영.숫자 포함 6자리 ~ 15자리 이내로 입력해주세요.';
        }
        if(pw.search(/₩s/) != -1){
          return '비밀번호는 공백없이 입력해주세요.';
        } 
        if(num < 0 || eng < 0){
          return '비밀번호는 영문.숫자를 혼합하여 입력해주세요.';
        }
        return 'good';
      };
  }

 closeModal() {
    this.viewCtrl.dismiss(this.loginParams.sc_flag);
  }
}