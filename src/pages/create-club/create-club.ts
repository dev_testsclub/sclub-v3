import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-create-club',
  templateUrl: 'create-club.html',
})
export class CreateClubPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateClubPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
