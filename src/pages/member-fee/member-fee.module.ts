import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MemberFeePage } from './member-fee';

@NgModule({
  declarations: [
    MemberFeePage,
  ],
  imports: [
    IonicPageModule.forChild(MemberFeePage),
  ],
  exports: [
    MemberFeePage
  ]
})
export class MemberFeePageModule {}
