import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetMyClubPage } from './set-my-club';

@NgModule({
  declarations: [
    SetMyClubPage,
  ],
  imports: [
    IonicPageModule.forChild(SetMyClubPage),
  ],
  exports: [
    SetMyClubPage
  ]
})
export class SetMyClubPageModule {}
