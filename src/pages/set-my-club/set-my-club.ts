import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { UserinfoProvider } from '../../providers/userinfo/userinfo';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-set-my-club',
  templateUrl: 'set-my-club.html',
})
export class SetMyClubPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public rest: RestProvider,
    public userInfo: UserinfoProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetMyClubPage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
