import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ActionSheetController } from 'ionic-angular';

import { UserinfoProvider } from '../../providers/userinfo/userinfo';
import { RestProvider } from '../../providers/rest/rest';
//import numeral from 'numeral';

@IonicPage()
@Component({
  selector: 'page-mypage',
  templateUrl: 'mypage.html',
})
export class MypagePage {

  login_flag: boolean;
  
  u_email:        any;
  u_password:     any;
  u_name:         any;
  u_phone_no:     any;
  u_birthday:     any;
  u_photo:        any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public actionSheetCtrl: ActionSheetController,
    public rest: RestProvider,
    public userInfo: UserinfoProvider) {
  }

  ionViewDidLoad() {
    this.u_email     = this.userInfo.userProfile.u_email;
    this.u_password  = this.userInfo.userProfile.u_password;
    this.u_name      = this.userInfo.userProfile.u_name;
    this.u_phone_no  = this.userInfo.userProfile.u_phone_no;
    this.u_birthday  = this.userInfo.userProfile.u_birthday;
    this.u_photo     = this.userInfo.userProfile.u_photo;
    console.log('ionViewDidLoad MypagePage');
  }
  ionViewDidEnter() {
    this.login_flag     = this.userInfo.userProfile.login_flag;
    console.log('ionViewDidLoad MypagePage');
  }

  logout() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: '로그아웃',
          role: 'destructive',
          handler: () => {
            this.userInfo.resetProfile();
            this.viewCtrl.dismiss();
            //this.navCtrl.setRoot(this.navCtrl.getActive().component);
            //this.app.getRootNav().getActiveChildNav().select(4);  // goto tab5Root - SetupPage
          }
        },{
          text: '취소',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
