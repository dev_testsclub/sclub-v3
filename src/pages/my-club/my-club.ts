import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, AlertController } from 'ionic-angular';

import { UserinfoProvider } from '../../providers/userinfo/userinfo';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-my-club',
  templateUrl: 'my-club.html',
})
export class MyClubPage {

  query_params: any;
  sclubURL = 'http://localhost:8080/mobile/';
  imgUrl = 'http://localhost:8080/mobile/img/';

  cc_id: any;
  club_id: any;
  club_name: any;
  club_emblem: any;
  ownerImgFlag: boolean = false;
  club_owner_email: any;
  club_owner_name: any;
  club_owner_image: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public rest: RestProvider, ) {

      this.club_id = navParams.get("club_id");

    }

  ionViewDidLoad() {
    this.query_params = 'CRUD_mode='+'read'+
                        '&club_id='+this.club_id;
    this.rest.CRUD_Club(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          this.cc_id = data.cc_id;
          this.club_name = data.club_name;
          this.club_owner_name = data.club_owner_name;
          if(data.club_owner_image == 'NaN') {
            this.ownerImgFlag = false;
          } else {          
            this.club_owner_image = this.imgUrl+'club_img/'+this.club_id+'/'+data.club_owner_image;
            this.ownerImgFlag = true;
          }
          this.club_owner_email = data.club_owner_email;
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
            });
          alert.present();
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get club Complete')
    );
    console.log('ionViewDidLoad MyClubPage');
  }

  club() {
    let ClubPage_modal = this.modalCtrl.create('ClubPage',{
      cc_id: this.cc_id,
      club_id: this.club_id
    });
    ClubPage_modal.present();
  }
  members() {
    //this.navCtrl.push('MembersPage');
    let MembersPage_modal = this.modalCtrl.create('MembersPage',{
      cc_id: this.cc_id,
      club_id: this.club_id
    });
    MembersPage_modal.present();
  }
  schedule() {
    //this.navCtrl.push('SchedulePage');
    let SchedulePage_modal = this.modalCtrl.create('SchedulePage',{
      cc_id: this.cc_id,
      club_id: this.club_id
    });
    SchedulePage_modal.present();
  }
  notification() {
    //this.navCtrl.push('NotificationPage');
    let NotificationPage_modal = this.modalCtrl.create('NotificationPage',{
      cc_id: this.cc_id,
      club_id: this.club_id
    });
    NotificationPage_modal.present();
  }
  vote() {
    //this.navCtrl.push('VotePage');
    let VotePage_modal = this.modalCtrl.create('VotePage',{
      cc_id: this.cc_id,
      club_id: this.club_id
    });
    VotePage_modal.present();    
  }
  teamSetup() {
    //this.navCtrl.push('TeamSetupPage');
    let TeamSetupPage_modal = this.modalCtrl.create('TeamSetupPage',{
      cc_id: this.cc_id,
      club_id: this.club_id
    });
    TeamSetupPage_modal.present();
  }
  memberFee() {
    //this.navCtrl.push('MemberFeePage');
    let MemberFeePage_modal = this.modalCtrl.create('MemberFeePage',{
      cc_id: this.cc_id,
      club_id: this.club_id
    });
    MemberFeePage_modal.present();
  }
  teamMatch() {
    //this.navCtrl.push('TeamMatchPage');
    let TeamMatchPage_modal = this.modalCtrl.create('TeamMatchPage',{
      cc_id: this.cc_id,
      club_id: this.club_id
    });
    TeamMatchPage_modal.present();
  }
  sessesion() {
    //this.navCtrl.push('SessesionPage');
    let SessesionPage_modal = this.modalCtrl.create('SessesionPage',{
      cc_id: this.cc_id,
      club_id: this.club_id
    });
    SessesionPage_modal.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
