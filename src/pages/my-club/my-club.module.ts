import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyClubPage } from './my-club';

@NgModule({
  declarations: [
    MyClubPage,
  ],
  imports: [
    IonicPageModule.forChild(MyClubPage),
  ],
  exports: [
    MyClubPage
  ]
})
export class MyClubPageModule {}
