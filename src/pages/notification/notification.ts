import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { UserinfoProvider } from '../../providers/userinfo/userinfo';

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  imgUrl = 'http://localhost:8080/mobile/img/';

  cc_id: any;
  club_id: any;

  //for message >>>>>>>>>>>>>>>>>>>>
  query_params: any;
  notices_msgs: any = []; //msg_division = '2'; //notices messages
  /* return data of getMessage
    club_id:"1"
    creator_image:"memberImg_icksykim@gmail.com_1479426098652.jpg"
    creator_name:"Kim Sooyoung"
    msg_attach:"1"
    msg_date:"2017-01-27"
    msg_division:"1"
    msg_file:"msgImg_icksykim@gmail.com_1485505043792.jpg"
    msg_id:"63"
    msg_like_cnt:"1"
    msg_memo:"우리조카 김주혜"
    msg_email:"010-3312-1234"
    */
    // "http://localhost:8080/mobile/img/club_img/{{m.club_id}}/{{m.creator_image}}"
    // "http://localhost:8080/mobile/img/club_img/{{m.club_id}}/{{m.msg_file}}"
  //for message <<<<<<<<<<<<<<<<<<<<<

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController, 
    public rest: RestProvider,
    public userInfo: UserinfoProvider) {
      this.cc_id = this.navParams.get("cc_id");
      this.club_id = this.navParams.get("club_id");
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }

  ionViewDidEnter() {
    this.getNotices();
  }

  getNotices() {
    this.query_params = 'GET_mode='       +'eachClub'+ 
                                '&club_id='       +this.club_id+
                                '&msg_division='	+'2'+ // '2' : notice 
                                '&req_switch='	  +'2';	// '2' : request from notice Board
    this.rest.getMessages(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          this.notices_msgs = data.message_board;
          for (let i = 0; i < this.notices_msgs.length; i++) {
            if(this.notices_msgs[i].creator_image == 'empty.png') {
              this.notices_msgs[i].creator_image = 'NaN';
            } else {
              this.notices_msgs[i].creator_image = this.imgUrl+'club_img/'+this.notices_msgs[i].club_id+'/'+this.notices_msgs[i].creator_image;
            }
            if(this.notices_msgs[i].msg_file == 'empty.png') {
              this.notices_msgs[i].msg_file = 'NaN';
            } else {
              this.notices_msgs[i].msg_file = this.imgUrl+'club_img/'+this.notices_msgs[i].club_id+'/'+this.notices_msgs[i].msg_file;
            }
          }
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get getGeneralMessage Complete')
    );
  }

  addNotice() {
    let NotificationCrudPage_modal = this.modalCtrl.create('NotificationCrudPage',{
      selectedNotice: 'add'
    });
    NotificationCrudPage_modal.present(); 
  }

  updateNotice(selectedNotice) {
    let NotificationCrudPage_modal = this.modalCtrl.create('NotificationCrudPage',{
      selectedNotice: selectedNotice
    });
    NotificationCrudPage_modal.present();      
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
