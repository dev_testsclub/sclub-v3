import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ModalController, PopoverController } from 'ionic-angular';
import { AbsoluteDragDirective } from '../../directives/absolute-drag/absolute-drag';

import { RestProvider } from '../../providers/rest/rest';
import { DeviceInfoProvider } from '../../providers/device-info/device-info';

@IonicPage()
@Component({
  selector: 'page-team-setup',
  templateUrl: 'team-setup.html',
})
export class TeamSetupPage {
  imgUrl = 'http://localhost:8080/mobile/img/';
  query_params: any;
  cc_id: any;
  cc_pitch_image: any;
  cc_name: any;
  club_id: any;
  t_id: any;

  team: any;
  teamPosition: any = [];
  po_resualt: any = [];
  /*
  $t->m_id = $row->m_id; 
  $t->m_email = $row->m_email; 
  $t->m_nick = $row->m_nick; 
  $t->m_pp_id = $row->m_pp_id; 
  $t->m_cp_id = $row->m_cp_id; 
  $t->m_back_no = $row->m_back_no; 
  $t->m_image = $row->m_image;
  $t->m_role_id = $row->m_role_id; 
  $t->m_admin = $row->m_admin; 
  $t->m_birthday = $row->m_birthday; 
  $t->m_phone = $row->m_phone; 
  $t->m_alarm = $row->m_alarm;
  $t->club_id = $row->club_id;
  
  $t->u_name = $row->u_name;	// member name
  $t->mrole_name = $row->mrole_name;	// member role name
  $t->u_birthday = $row->u_birthday;	// member birthday

  $t->m_pp_name = $p_row['p_full_name'];
  $t->m_cp_name = $c_row['p_full_name'];
*/
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public rest: RestProvider,
    public deviceInfo: DeviceInfoProvider,
    public modalCtrl: ModalController,
    private popoverCtrl: PopoverController) {
      this.cc_id = this.navParams.get("cc_id");
      this.club_id = this.navParams.get("club_id");
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeamSetupPage');
  }

  ionViewDidEnter() {
    for (let i = 0; i < this.rest.category.length; i++) {
      if(this.rest.category[i].cc_id == this.cc_id) {
        this.cc_pitch_image = this.rest.category[i].cc_pitch_image;
        this.cc_name = this.rest.category[i].cc_name;
      }
    }
    //this.getTeams();
    console.log('ionViewDidEnter TeamSetupPage');
  }

  getTeamPosition() {
    this.query_params = 'cc_name=' +this.cc_name+ '&club_id=' +this.club_id+ '&t_id=' +this.team.t_id+ '&f_id=' +this.team.f_id;

    this.rest.getTeamPosition(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          this.teamPosition = data.team_position;
          let po_array = [];
          for (let i=0; i<Object.keys(this.teamPosition).length; i++){
            /*
            this.teamPosition[i].po_x = (this.teamPosition[i].po_x * this.xRate).toFixed(0);
            this.teamPosition[i].po_y = (this.teamPosition[i].po_y * this.yRate).toFixed(0);
            */
            this.teamPosition[i].st_po_img = this.imgUrl+this.teamPosition[i].st_po_img;
            this.teamPosition[i].po_r = {'width': '15%', 
                                         'height': 'auto', 
                                         'left.px': this.teamPosition[i].po_x, 
                                         'top.px': this.teamPosition[i].po_y}
                                        /*
                                        'left.px': (this.teamPosition[i].po_x * this.deviceInfo.deviceInfo.d_width_r).toFixed(0), 
                                         'top.px': (this.teamPosition[i].po_y * this.deviceInfo.deviceInfo.d_height_r).toFixed(0)}
                                        */
            po_array[i] = {'po_name' : this.teamPosition[i].po_short_name, 
                           'po_x' : parseInt(this.teamPosition[i].po_x), 
                           'po_y' : parseInt(this.teamPosition[i].po_y),
                           't_id' : this.teamPosition[i].t_id,
                           'mpo_id' : this.teamPosition[i].mpo_id};
          }
          this.po_resualt = po_array;
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get getTeams Complete')
    );
  }

  selectTeam() {
    let popover = this.popoverCtrl.create('PopoverPage', { club_id: this.club_id });
    popover.onDidDismiss(data => {
      if(data != null) {
        this.team = data;
        this.getTeamPosition();
      }
    });
    popover.present();
  }
  saveTeam() {}
  addTeam() {}
  deleteTeam() {}

  assignPosition(m_id) {
    let AssignPositionPage_modal = this.modalCtrl.create('AssignPositionPage',{
      club_id: this.club_id,
      m_id: m_id
    });
    AssignPositionPage_modal.onDidDismiss(data => {
      if(data == true) {
        //this.getTeam();
      }
    });
    AssignPositionPage_modal.present();
  }
  panEvent(e) {
    console.log('Move image with panEvent');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
