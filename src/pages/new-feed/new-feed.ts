import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { UserinfoProvider } from '../../providers/userinfo/userinfo';

@IonicPage()
@Component({
  selector: 'page-new-feed',
  templateUrl: 'new-feed.html',
})
export class NewFeedPage {

  imgUrl = 'http://localhost:8080/mobile/img/';
  message_query_params: any;
  all_msgs: any = [];     //msg_division = '1' & '2' & allClubs; //all messages

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public rest: RestProvider,) {
  }

  ionViewDidLoad() {
    // for message
    this.message_query_params = 'GET_mode=' +'allClubs';
    this.rest.getMessages(this.message_query_params).subscribe(
      data => {
        if(data.result == true) {
          this.all_msgs = data.message_board;
          for (let i = 0; i < this.all_msgs.length; i++) {
            if(this.all_msgs[i].creator_image == 'empty.png') {
              this.all_msgs[i].creator_image = 'NaN';
            } else {
              this.all_msgs[i].creator_image = this.imgUrl+'club_img/'+this.all_msgs[i].club_id+'/'+this.all_msgs[i].creator_image;
            }
            if(this.all_msgs[i].msg_file == 'empty.png') {
              this.all_msgs[i].msg_file = 'NaN';
            } else {
              this.all_msgs[i].msg_file = this.imgUrl+'club_img/'+this.all_msgs[i].club_id+'/'+this.all_msgs[i].msg_file;
            }
          }
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get getGeneralMessage Complete')
    );
    console.log('ionViewDidLoad NewFeedPage');
  }
  searchClub() {
    
  }
  addMessage() {
    // need popup for select club
    // and modal for create message
  }

}
