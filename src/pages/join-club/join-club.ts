import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-join-club',
  templateUrl: 'join-club.html',
})
export class JoinClubPage {

  clubs: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public rest: RestProvider,) {
  }

  ionViewDidLoad() {
    //this.clubs = this.rest.clubs;
    console.log('ionViewDidLoad JoinClubPage');
  }

  initializeClubs() {
    //this.clubs = this.rest.clubs;
  }
  onInput(ev) {
    // Reset items back to all of the items
    this.initializeClubs();
    // set val to the value of the ev target
    var val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.clubs.club_name = this.clubs.club_name.filter((item) => {
        return (this.clubs.club_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  /*
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  */
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
