import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JoinClubPage } from './join-club';

@NgModule({
  declarations: [
    JoinClubPage,
  ],
  imports: [
    IonicPageModule.forChild(JoinClubPage),
  ],
  exports: [
    JoinClubPage
  ]
})
export class JoinClubPageModule {}
