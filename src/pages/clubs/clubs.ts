import { Component,ViewChild, ElementRef } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

import { RestProvider } from '../../providers/rest/rest';

declare var google;

@IonicPage()
@Component({
  selector: 'page-clubs',
  templateUrl: 'clubs.html',
})
export class ClubsPage {
  
  @ViewChild('map') mapElement: ElementRef;
  
  map: any;
  gpsFlag: boolean;
  imgUrl = 'http://localhost:8080/mobile/img/';
  query_params: any;

  clubs: any = [];
  footballClubs: any = [];    // cc_id:"8"  football
  baseballClubs: any = [];    // cc_id:"10" baseball
  basketballClubs: any = [];  // cc_id:"11" basketball
  tennisClubs: any = [];      // cc_id:"12" tennis

  category: string = "football";
  
  
  /*
  cc_id:"8" 8:football, 10:baseball, 11:basketball, 12:tennis
  club_address:"대한민국 서울특별시 광진구 자양1동 능동로 120"
  club_contact_name:"Kim Sooyoung"
  club_contact_email:"010-3312-1234"
  club_emblem:"clubImg_1479113025131.jpg"
  club_id:"12"
  club_intro:"건국대학교 정보통신대학 컴퓨터공학과 축구클럽"
  club_loc_lat:"37.5407625"
  club_loc_lng:"127.07934279999995"
  club_location:"건국대학교"
  club_name:"CE FC"
  club_open_flag:"fo"
  club_owner:"01044444444"
  club_owner_name:"YAWH004"
  club_purcAmount:null
  club_purcDate:null
  club_purcPhone:null
  club_purcStatus:"o"
  club_regurations:"건국대학교 정보통신대학 컴퓨터공학과 축구클럽"
  member_cnt:"16"
  */
  constructor(
    public platform: Platform,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public rest: RestProvider,
    private geolocation: Geolocation) {
    
  }

  ionViewDidLoad() {

    this.query_params = 'GET_mode='+'allCategory';   //GET_mode: allCategory, all+cc_id, search+cc_id+club_address
    this.rest.getClubs(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          this.clubs = data.clubs;
          for (let i = 0; i < this.clubs.length; i++) {
            if(this.clubs[i].club_emblem != 'NaN') {
              this.clubs[i].club_emblem = this.imgUrl+'club_img/'+this.clubs[i].club_id+'/'+this.clubs[i].club_emblem;
            }
          }
          for (let i = 0; i < this.clubs.length; i++) {
            if(this.clubs[i].cc_id == '8') {              // 8:football
              this.footballClubs.push(this.clubs[i]);
            } else if(this.clubs[i].cc_id == '10') {      // 10:baseball
              this.baseballClubs.push(this.clubs[i]);
            } else if(this.clubs[i].cc_id == '11') {      // 11:basketball
              this.basketballClubs.push(this.clubs[i]);
            } else {                                      // 12:tennis
              this.tennisClubs.push(this.clubs[i]);
            }
          }
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get getClubs Complete')
    );
    
    //this.loadMap(this.category);
    this.checkLocationService();
    console.log('ionViewDidLoad ClubsPage');
  }

  checkLocationService(){
    let options = {timeout: 2000, enableHighAccuracy: true};
    this.geolocation.getCurrentPosition(options).then((resp) => {
      this.gpsFlag = true;
      //console.log('LocationService ON: '+this.gpsFlag);
      this.loadMap(this.category);
    }).catch((error) => {
      this.gpsFlag = false;
      //console.log('LocationService OFF: '+this.gpsFlag);
      if(this.platform.is('ios')) {
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '지도에서 위치를 확인하려면 휴대폰의 설정->개인정보보호 에서 "위치서비스"를 켜야합니다.',
          buttons: ['확인']
        });
        alert.present();
      } else if(this.platform.is('android')) {
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '지도에서 위치를 확인하려면 휴대폰에서 "위치서비스"를 켜야합니다.',
          buttons: ['확인']
        });
        alert.present();
      } else {
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '지도에서 위치를 확인하려면 "위치서비스"를 켜야합니다.',
          buttons: ['확인']
        });
        alert.present();
      }
    });
  }


  loadMap(category){ 
    this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        fullscreenControl: false
      }
      const element: HTMLElement = document.getElementById('cmap');
      this.map = new google.maps.Map(element, mapOptions);
      let infowindow = [];
      if(this.category == 'football') {
        for(let i = 0; i < this.footballClubs.length; i++){
          let lat = parseFloat(this.footballClubs[i].club_loc_lat);
          let lng = parseFloat(this.footballClubs[i].club_loc_lng);
          infowindow[i] = new google.maps.InfoWindow();
          let marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(lat, lng)
          });
          marker.setMap(this.map);
          let env = this;
          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow[i].setContent('<div><strong>' + env.footballClubs[i].club_name + '</strong><br>' +
                      'Location : ' + env.footballClubs[i].club_location + '<br>' +
                      'Contact : ' + env.footballClubs[i].club_contact_email + '<br></div>');
              infowindow[i].open(env.map, marker);
            }
          })(marker, i));
        }
      } else if(this.category == 'baseball') {
        for(let i = 0; i < this.baseballClubs.length; i++){
          let lat = parseFloat(this.baseballClubs[i].club_loc_lat);
          let lng = parseFloat(this.baseballClubs[i].club_loc_lng);
          infowindow[i] = new google.maps.InfoWindow();
          let marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(lat, lng)
          });
          marker.setMap(this.map);
          let env = this;
          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow[i].setContent('<div><strong>' + env.baseballClubs[i].club_name + '</strong><br>' +
                      'Location : ' + env.baseballClubs[i].club_location + '<br>' +
                      'Contact : ' + env.baseballClubs[i].club_contact_email + '<br></div>');
              infowindow[i].open(env.map, marker);
            }
          })(marker, i));
        }
      } else if(this.category == 'basketball') {
        for(let i = 0; i < this.basketballClubs.length; i++){
          let lat = parseFloat(this.basketballClubs[i].club_loc_lat);
          let lng = parseFloat(this.basketballClubs[i].club_loc_lng);
          infowindow[i] = new google.maps.InfoWindow();
          let marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(lat, lng)
          });
          marker.setMap(this.map);
          let env = this;
          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow[i].setContent('<div><strong>' + env.basketballClubs[i].club_name + '</strong><br>' +
                      'Location : ' + env.basketballClubs[i].club_location + '<br>' +
                      'Contact : ' + env.basketballClubs[i].club_contact_email + '<br></div>');
              infowindow[i].open(env.map, marker);
            }
          })(marker, i));
        }
      } else {
        for(let i = 0; i < this.tennisClubs.length; i++){
          let lat = parseFloat(this.tennisClubs[i].club_loc_lat);
          let lng = parseFloat(this.tennisClubs[i].club_loc_lng);
          infowindow[i] = new google.maps.InfoWindow();
          let marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(lat, lng)
          });
          marker.setMap(this.map);
          let env = this;
          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow[i].setContent('<div><strong>' + env.tennisClubs[i].club_name + '</strong><br>' +
                      'Location : ' + env.tennisClubs[i].club_location + '<br>' +
                      'Contact : ' + env.tennisClubs[i].club_contact_email + '<br></div>');
              infowindow[i].open(env.map, marker);
            }
          })(marker, i));
        }
      }
    }, (err) => {
      console.log(err);
    });
  }

}
