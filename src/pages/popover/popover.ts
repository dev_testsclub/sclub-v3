import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, AlertController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {
  club_id: any;
  query_params: any;
  teams: any = [];
  
  constructor(
    public navParams: NavParams, 
    public alertCtrl: AlertController,
    private viewCtrl: ViewController,  
    public rest: RestProvider,) {
      this.club_id = this.navParams.get("club_id");
      this.getTeams();
      console.log('Hello PopoverComponent Component');
  }
  getTeams() {
    this.query_params = 'club_id='+this.club_id;
    this.rest.getTeams(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          this.teams = data.teams;
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
          this.viewCtrl.dismiss();
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Get getTeams Complete')
    );
  }

  selectedTeam(team) {
    this.viewCtrl.dismiss(team);
  }
}
