import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamMatchPage } from './team-match';

@NgModule({
  declarations: [
    TeamMatchPage,
  ],
  imports: [
    IonicPageModule.forChild(TeamMatchPage),
  ],
  exports: [
    TeamMatchPage
  ]
})
export class TeamMatchPageModule {}
