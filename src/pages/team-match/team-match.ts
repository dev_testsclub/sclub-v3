import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-team-match',
  templateUrl: 'team-match.html',
})
export class TeamMatchPage {

  cc_id: any;
  club_id: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,) {
      this.cc_id = this.navParams.get("cc_id");
      this.club_id = this.navParams.get("club_id");
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeamMatchPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
