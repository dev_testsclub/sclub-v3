import { Component } from '@angular/core';
import { IonicPage, App, NavController, NavParams, ViewController, ModalController, AlertController } from 'ionic-angular';

import { UserinfoProvider } from '../../providers/userinfo/userinfo';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-sessesion',
  templateUrl: 'sessesion.html',
})
export class SessesionPage {

  query_params: any;
  imgUrl = 'http://localhost:8080/mobile/img/';
  cc_id: any;
  club_id: any;
  club_name: any;
  club_emblem: any;
  clubImgFlag: boolean = false;

  constructor(
    public app: App, 
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public rest: RestProvider,) {
      this.cc_id = this.navParams.get("cc_id");
      this.club_id = this.navParams.get("club_id");
    }

  ionViewDidLoad() {
      this.query_params = 'CRUD_mode='+'read'+
                          '&club_id='+this.club_id;
    this.rest.CRUD_Club(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          this.club_name = data.club_name;
          this.club_emblem = data.club_emblem;
          if(this.club_emblem == 'NaN') {
            this.clubImgFlag = false;
          } else {          
            this.club_emblem = this.imgUrl+'club_img/'+this.club_id+'/'+this.club_emblem;
            this.clubImgFlag = true;
          }
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('get Club Complete')
    ); 
    console.log('ionViewDidLoad SessesionPage');
  }

  secession() {
    this.query_params = 'CRUD_mode='+'delete'+
                        '&club_id='+this.club_id;
    this.rest.CRUD_Club(this.query_params).subscribe(
      data => {
        if(data.result == true) {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
          //this.viewCtrl.dismiss();
          this.app.getRootNav().getActiveChildNav().select(2);  // goto tab2Root - HomePage
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: data.msg,
            buttons: ['확인']
          });
          alert.present();
        }
      },
      err => {
        console.log(err);
      },
      () => console.log('Club secession Complete')
    ); 
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
