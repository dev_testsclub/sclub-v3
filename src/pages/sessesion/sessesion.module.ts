import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SessesionPage } from './sessesion';

@NgModule({
  declarations: [
    SessesionPage,
  ],
  imports: [
    IonicPageModule.forChild(SessesionPage),
  ],
  exports: [
    SessesionPage
  ]
})
export class SessesionPageModule {}
